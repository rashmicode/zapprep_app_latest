package in.zapprep.ZapPrep;

import android.content.Context;
import android.util.Log;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by PoolDead on 11/24/2017.
 */

public class UpdateMentorOnlineStatus {
    private String access_token;
    private AppPrefs appPrefs;

    public UpdateMentorOnlineStatus(Context c){
        appPrefs = new AppPrefs(c);
        access_token = appPrefs.getAuthCode();
    }

    public void updateActiveStatus(Context context){
        RestClient.get(context).postOnlineStatusActive(access_token, new Callback<StatusResponseData>() {
            @Override
            public void success(StatusResponseData statusResponseData, Response response) {
                String message = statusResponseData.getMessage();
                if(message.contentEquals("success")){
                    //Log.e("yash","Update Active Status : Success");
                }else{
                    Log.e("yash","Update Active Status : Failed");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("yash","Update Active Status : Retrofit Error "+ error.toString());
            }
        });
    }

    public void updateInActiveStatus(Context context){
        RestClient.get(context).postOnlineStatusInActive(access_token, new Callback<StatusResponseData>() {
            @Override
            public void success(StatusResponseData statusResponseData, Response response) {
                String message = statusResponseData.getMessage();
                if(message.contentEquals("success")){
                    //Log.e("yash","Update InActive Status : Success");
                }else{
                    Log.e("yash","Update InActive Status : Failed");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("yash","Update InActive Status : Retrofit Error "+ error.toString());
            }
        });
    }
}
