package in.zapprep.ZapPrep;

/**
 * Created by PoolDead on 12/26/2017.
 */

public class InviteResponse {
    boolean invite;

    public boolean isInvite() {
        return invite;
    }

    public void setInvite(boolean invite) {
        this.invite = invite;
    }

}
