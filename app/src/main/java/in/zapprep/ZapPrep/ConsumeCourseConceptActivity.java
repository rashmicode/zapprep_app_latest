package in.zapprep.ZapPrep;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ListFragment;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ResolveInfo;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.android.youtube.player.YouTubeApiServiceUtil;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubeStandalonePlayer;
import in.zapprep.ZapPrep.Notifications.NotificationActivity;
//import com.quickblox.chat.model.QBDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A sample Activity showing how to manage multiple YouTubeThumbnailViews in an adapter for display
 * in a List. When the list items are clicked, the video is played by using a YouTubePlayerFragment.
 * <p/>
 * The demo supports custom fullscreen and transitioning between portrait and landscape without
 * rebuffering.
 */
@TargetApi(13)
public final class ConsumeCourseConceptActivity extends NavigationActivity {
    private static final int REQ_START_STANDALONE_PLAYER = 1;
    private static final int REQ_RESOLVE_SERVICE_MISSING = 2;

    /**
     * The request code when calling startActivityForResult to recover from an API service error.
     */
    private static final int RECOVERY_DIALOG_REQUEST = 1;

    private VideoListFragment listFragment;
    
    // Course Concept Variables
    public DrawerLayout mLeftDrawerLayout;
    private Toolbar appBar;
    int position = 0;
    List<ConceptData> listConceptData;
    private Boolean open = false;
    AppPrefs appPrefs;
    Menu mMenu;
    String mPathId;
//    DialogsAdapter mDialogsAdapter;
    static ConceptAdapter conceptAdapter;
    PeoplePageActivityFragment peoplePageActivityFragment;
    static TextView mNoResult;
    static Activity conceptObjectActivity;
    int pos;
    Button courseShareButton;
    JSONObject propsChannel;
    JSONObject propsHamburger;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consume_course_concept);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        // APPBAR Code
        conceptObjectActivity = this;
        mNoResult = (TextView) findViewById(R.id.noMatch);
        appBar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(appBar);
        appPrefs = new AppPrefs(this);
        //Drawer Layout
        //mLeftDrawerLayout = (DrawerLayout) mfullLayout.findViewById(R.id.drawer_layout);
        mLeftDrawerLayout=super.mDrawerLayout;

        appBar.setNavigationIcon(R.drawable.pyoopil_logo_white);
        appBar.setNavigationContentDescription("BACK");
        ConsumeCourseConceptActivity.this.setTitle(ConsumeCoursePage.title);

        final List<String> listItems = new ArrayList<>();

        // Getting th position from Course Page
        if (getIntent().getExtras() != null) {
            mPathId = getIntent().getExtras().getString("pathID");
            position = getIntent().getIntExtra("position", 0);
        }
        propsChannel = new JSONObject();
        listItems.add("Review");

        View spinnerContainer = LayoutInflater.from(this).inflate(R.layout.toolbar_spinner,
                appBar, false);
        ActionBar.LayoutParams lp = new ActionBar.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        appBar.addView(spinnerContainer, lp);

        final Spinner spinner = (Spinner) spinnerContainer.findViewById(R.id.toolbar_spinner);
        SpinnerAdapter spinnerAdapter = new SpinnerAdapter(this, ConsumeCoursePage.title, mPathId, spinner, "objectPage");
        spinnerAdapter.addItems(listItems);
        spinner.setAdapter(spinnerAdapter);
//        pos = spinner.getSelectedItemPosition();
       // Log.e("", "The count is" + pos);
//        spinner.setOnItemSelectedListener(this);
        appBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLeftDrawerLayout.openDrawer(GravityCompat.START);
            }
        });

        mLeftDrawerLayout.setDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {

            }

            @Override
            public void onDrawerOpened(View drawerView) {
                hideSoftKeyboard();
            }

            @Override
            public void onDrawerClosed(View drawerView) {

            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });

        listFragment = (VideoListFragment) getFragmentManager().findFragmentById(R.id.list_fragment);


        // Getting the list from List adapter
        listConceptData = ListAdapter.list;
        ConceptData conceptData = listConceptData.get(position);

        // Setting the title text for conceptTile
        TextView conceptTitle = (TextView) findViewById(R.id.conceptTitle);
        conceptTitle.setText(conceptData.getTitle());
        conceptTitle.setTypeface(ApplicationSingleton.robotoRegular);
        //Redirect to Consume Course and People Page

        courseShareButton = (Button) findViewById(R.id.course_shareButton);

        courseShareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Event -ShareCourse
                JSONObject propsShare = new JSONObject();
                try {
                    propsShare.put("Title", DashBoardActivity.listDash.get(ApplicationUtility.getListPositionFromPathID(mPathId, "redirectDashBoard")).getTitle());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                mixpanel.track("ShareCourse", propsShare);
                String url;
                if (DashBoardActivity.listDash.get(ApplicationUtility.getListPositionFromPathID(mPathId, "redirectDashBoard")).getProducer() != null) {
                    url = "http://www.pyoopil.com/profile/" + DashBoardActivity.listDash.get(ApplicationUtility.getListPositionFromPathID(mPathId, "redirectDashBoard")).getProducer().getId().trim() + "/" + DashBoardActivity.listDash.get(ApplicationUtility.getListPositionFromPathID(mPathId, "redirectDashBoard")).getProducer().getUsername().trim() + "/course/" + mPathId + "/" + DashBoardActivity.listDash.get(ApplicationUtility.getListPositionFromPathID(mPathId, "redirectDashBoard")).getTitle().trim();
                } else {
                    url = "http://www.pyoopil.com/profile/" + appPrefs.getUser_Id() + "/" + appPrefs.getUser_Name() + "/course/" + mPathId + "/" + DashBoardActivity.listDash.get(ApplicationUtility.getListPositionFromPathID(mPathId, "redirectDashBoard")).getTitle().trim();
                }
                String appUrl = "https://goo.gl/AjwSVn";
                String replacedUrl = url.replaceAll(" ", "-");
                Log.e("Explore Course", "Share button clicked");
                Intent i = new Intent(android.content.Intent.ACTION_SEND);
                i.setType("text/plain");
                i.putExtra(android.content.Intent.EXTRA_TEXT, "Hey! I am taking a course on Pyoopil - " + DashBoardActivity.listDash.get(ApplicationUtility.getListPositionFromPathID(mPathId, "redirectDashBoard")).getTitle() + ". Download the app from this link -\n" + appUrl + "\n\nand enroll into this course - \n" + replacedUrl);
                       /* marketData.putExtra(android.content.Intent.EXTRA_SUBJECT, "The course details are : ");
                        marketData.putExtra(android.content.Intent.EXTRA_TEXT, replacedUrl + "\n");*/
                startActivity(Intent.createChooser(i, "Share via"));
            }
        });

       /* //Starred Channel
        View starredChannel = findViewById(R.id.rd_chat_starred);
        starredChannel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StarredFragment newStarredFragment = StarredFragment.newInstance(mGridPosition);
                getSupportFragmentManager().beginTransaction().replace(R.id.center_container, newStarredFragment).commit();
                mRightDrawerLayout.closeDrawer(Gravity.RIGHT);
            }
        });*/

        checkYouTubeApi();
    }

//    private void unSetUpar() {
//        unSetRooms(false);
//    }
//
//    private void unSetAll() {
//        unSetRooms(true);
//    }
//
//    private void unSetRooms(boolean flag) {
//        if (flag) {
////            mDialogsAdapter.notifyDataSetChanged();
//        }
//    }

    private void checkYouTubeApi() {
        YouTubeInitializationResult errorReason =
                YouTubeApiServiceUtil.isYouTubeApiServiceAvailable(this);
        if (errorReason.isUserRecoverableError()) {
            errorReason.getErrorDialog(this, RECOVERY_DIALOG_REQUEST).show();
        } else if (errorReason != YouTubeInitializationResult.SUCCESS) {
            String errorMessage =
                    String.format(getString(R.string.error_player), errorReason.toString());
            Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQ_START_STANDALONE_PLAYER && resultCode != RESULT_OK) {
            YouTubeInitializationResult errorReason =
                    YouTubeStandalonePlayer.getReturnedInitializationResult(data);
            if (errorReason.isUserRecoverableError()) {
                errorReason.getErrorDialog(this, 0).show();
            } else {
                String errorMessage =
                        String.format(getString(R.string.error_player), errorReason.toString());
                Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onBackPressed() {
//        ChatFragment test = (ChatFragment) getSupportFragmentManager().findFragmentByTag("chatFragment");
//        AnnouncementsFragment test1 = (AnnouncementsFragment) getSupportFragmentManager().findFragmentByTag("announceFrag");
//        PeoplePageActivityFragment test2 = (PeoplePageActivityFragment) getSupportFragmentManager().findFragmentByTag("peopleFrag");
//        CourseRatingFragment courseRatingFragment = (CourseRatingFragment) getSupportFragmentManager().findFragmentByTag("objectPage");
//        if (test != null && test.isVisible() || test1 != null && test1.isVisible() || test2 != null && test2.isVisible() || courseRatingFragment != null && courseRatingFragment.isVisible()) {
//            Intent consumeIntent = new Intent(ConsumeCourseConceptActivity.this, ConsumeCourseConceptActivity.class);
//            consumeIntent.putExtra("pathID", mPathId);
//            consumeIntent.putExtra("position", position);
//            startActivity(consumeIntent);
//            //DO STUFF
//        } else {
            //Whatever
            Intent dashIntent = new Intent(ConsumeCourseConceptActivity.this, ConsumeCoursePage.class);
//            dashIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            dashIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            dashIntent.putExtra("pathID", mPathId);
            startActivity(dashIntent);
//        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_rightham_2, menu);
        mMenu = menu;

        if (peoplePageActivityFragment != null) {
            menu.findItem(R.id.action_search_concept).setVisible(false);
        }

        // Associate searchable configuration with the SearchView
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView =
                (SearchView) menu.findItem(R.id.action_search_concept).getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));

        SearchView.OnQueryTextListener textChangeListener = new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextChange(String newText) {
                // this is your adapter that will be filtered
                if (newText.length() < 5 || newText.contains(" ")) {
                    conceptAdapter.getFilter().filter(newText);
                    Log.e("on text chnge text: ", "" + newText);
                }
                return false;
            }

            @Override
            public boolean onQueryTextSubmit(String query) {
                // this is your adapter that will be filtered
                Log.e("on query submit: ", "" + query);
                conceptAdapter.getFilter().filter(query);

                return true;
            }
        };
        searchView.setOnQueryTextListener(textChangeListener);

        final TextView count_textview;
        Firebase fireBaseCountRef;

        final View menu_notification = menu.findItem(R.id.notificationicon).getActionView();
        count_textview = (TextView) menu_notification.findViewById(R.id.tv_unread_count);
        count_textview.setVisibility(View.INVISIBLE);
        Firebase.setAndroidContext(getApplicationContext());
        fireBaseCountRef = new Firebase(ApplicationSingleton.FIREBASE_URL + "users/" + appPrefs.getUser_Id() + "/");

        fireBaseCountRef.child("count").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() == null) {
                    Log.e("", "Its empty bro in consume course");
                } else {
                    countNot = Integer.parseInt(dataSnapshot.getValue().toString());
                }
                ApplicationUtility.updateUnreadCount(countNot, ConsumeCourseConceptActivity.this, count_textview);
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });


        menu_notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ConsumeCourseConceptActivity.this, NotificationActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_from_bottom, R.anim.stay);

                Firebase changeValues = new Firebase(ApplicationSingleton.FIREBASE_URL + "users/" + appPrefs.getUser_Id() + "/");
                changeValues.child("count").setValue(0l);
            }
        });


        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        return super.onOptionsItemSelected(item);
    }

    /**
     * A fragment that shows a list of all list of concepts.(Text,Video,Link,Image,File,Quiz)
     */
    public static final class VideoListFragment extends ListFragment {


        // Course Concept Fragment Variables
        int position = 0;
        List<ConceptData> listConceptData;

        List<ConceptObjects> conceptObjectsList;
        JSONObject propsCourseObject;

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(), "Lato-Regular.ttf");

            listConceptData = ListAdapter.list;
            position = getActivity().getIntent().getIntExtra("position", 0);
            ConceptData conceptData = listConceptData.get(position);
            conceptObjectsList = conceptData.getObjects();
            propsCourseObject = new JSONObject();

            conceptAdapter = new ConceptAdapter(ApplicationSingleton.context, conceptObjectsList, mNoResult);

        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);


            getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);
            getListView().setDivider(null);
            setListAdapter(conceptAdapter);
        }

        @Override
        public void onListItemClick(ListView l, View v, int position, long id) {


            Intent intent = null;

            ConceptObjects conceptObject = conceptObjectsList.get(position);
            ConceptAdapter.TypeTag type = (ConceptAdapter.TypeTag) v.getTag();


            switch (type.type) {
                case "video":
                    //MixPanel Integration. Event - CourseObject

                    try {
                        propsCourseObject.put("ObjectType", "video");

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    mixpanel.track("CourseObject", propsCourseObject);
                    String youtubeURL = conceptObject.getUrl();
                    String videoID = getYoutubeVideoID(youtubeURL);
                    Log.e("", "the id is" + videoID);

                    if (videoID == null) {
                        Toast.makeText(getActivity(), "Sorry. There seems to be a problem with the video.", Toast.LENGTH_SHORT).show();
                    } else {
                        intent = YouTubeStandalonePlayer.createVideoIntent(
                                getActivity(), DeveloperKey.DEVELOPER_KEY, videoID, 00, true, false);


                        if (intent != null) {
                            if (canResolveIntent(intent)) {
                                startActivityForResult(intent, REQ_START_STANDALONE_PLAYER);
                            } else {
                                // Could not resolve the intent - must need to install or update the YouTube API service.
                                YouTubeInitializationResult.SERVICE_MISSING
                                        .getErrorDialog(getActivity(), REQ_RESOLVE_SERVICE_MISSING).show();
                            }
                        }
                    }
                    break;

                case "text":
                    try {
                        propsCourseObject.put("ObjectType", "text");

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    mixpanel.track("CourseObject", propsCourseObject);
                    intent = new Intent(getActivity(), TextObjectActivity.class);
                    intent.putExtra("title", conceptObject.getTitle());
                    intent.putExtra("desc", conceptObject.getDesc());
                    startActivity(intent);

                    break;

                case "quiz":
                    try {
                        propsCourseObject.put("ObjectType", "quiz");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    mixpanel.track("CourseObject", propsCourseObject);
                    intent = new Intent(getActivity(), QuizObjectActivity.class);
                    intent.putExtra("title", conceptObject.getTitle());
                    intent.putExtra("objId", conceptObject.getId());
                    startActivity(intent);

                    break;

                case "image":
                    try {
                        propsCourseObject.put("ObjectType", "image");

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    mixpanel.track("CourseObject", propsCourseObject);
                    intent = new Intent(getActivity(), ImageObjectActivity.class);
                    intent.putExtra("title", conceptObject.getTitle());
                    intent.putExtra("desc", conceptObject.getDesc());
                    intent.putExtra("image", conceptObject.getUrl());
                    startActivity(intent);
                    break;
            }
        }

        private boolean canResolveIntent(Intent intent) {
            List<ResolveInfo> resolveInfo = getActivity().getPackageManager().queryIntentActivities(intent, 0);
            return resolveInfo != null && !resolveInfo.isEmpty();
        }

        private String getYoutubeVideoID(String url) {

            String pattern = "(?<=watch\\?v=|/videos/|embed\\/|youtu.be\\/|\\/v\\/|watch\\?v%3D|%2Fvideos%2F|embed%‌​2F|youtu.be%2F|%2Fv%2F)[^#\\&\\?\\n]*";

            Pattern compiledPattern = Pattern.compile(pattern);
            Matcher matcher = compiledPattern.matcher(url);

            if (matcher.find()) {
                return matcher.group();
            }
            return null;
        }

        @Override
        public void onDestroyView() {
            super.onDestroyView();
            conceptAdapter.releaseLoaders();
        }
    }

//    @Override
//    public void disableDrawer(Boolean disable) {
//        if (disable) {
//            //mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
//            mLeftDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
//            appBar.setNavigationOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                }
//            });
//
//        } else {
//           //mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
//            mLeftDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
//            appBar.setNavigationOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    mLeftDrawerLayout.openDrawer(GravityCompat.START);
//                }
//            });
//
//        }
//    }

    public void hideSoftKeyboard() {
        if (this.getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
        }
    }

}

