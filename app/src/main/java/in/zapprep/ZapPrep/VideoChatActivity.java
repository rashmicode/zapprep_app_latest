package in.zapprep.ZapPrep;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.vidyo.VidyoClient.Connector.Connector;
import com.vidyo.VidyoClient.Connector.VidyoConnector;
import com.vidyo.VidyoClient.Device.VidyoDevice;
import com.vidyo.VidyoClient.Device.VidyoLocalCamera;
import com.vidyo.VidyoClient.Device.VidyoLocalMicrophone;
import com.vidyo.VidyoClient.Device.VidyoLocalSpeaker;
import com.vidyo.VidyoClient.Device.VidyoRemoteCamera;
import com.vidyo.VidyoClient.Endpoint.VidyoLogRecord;
import com.vidyo.VidyoClient.Endpoint.VidyoParticipant;
import com.vidyo.VidyoClient.VidyoNetworkInterface;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import in.zapprep.ZapPrep.Notifications.NotificationActivity;

import static in.zapprep.ZapPrep.R.id.remote;

public class VideoChatActivity extends NavigationActivity implements
        VidyoConnector.IConnect,
        VidyoConnector.IRegisterLogEventListener,
        VidyoConnector.IRegisterNetworkInterfaceEventListener,
        VidyoConnector.IRegisterLocalCameraEventListener,
        VidyoConnector.IRegisterRemoteCameraEventListener,
        VidyoConnector.IRegisterLocalMicrophoneEventListener,
        VidyoConnector.IRegisterLocalSpeakerEventListener,
        VidyoConnector.IRegisterParticipantEventListener{

    enum VIDYO_CONNECTOR_STATE {
        VC_CONNECTED,
        VC_DISCONNECTED,
        VC_DISCONNECTED_UNEXPECTED,
        VC_CONNECTION_FAILURE
    }

    DrawerLayout mLeftDrawerLayout;
    static String mPathId;
    private Toolbar mAppBar;
    private AppPrefs appPrefs;
    ProgressDialog dialog;
    Menu mMenu;

    private VIDYO_CONNECTOR_STATE mVidyoConnectorState = VIDYO_CONNECTOR_STATE.VC_DISCONNECTED;
    private boolean mVidyoClientInitialized = false;
    private VidyoConnector mVidyoConnector = null;
    private ToggleButton mToggleConnectButton;
    private ToggleButton mToggleCameraSwitchButton;
    private ToggleButton mToggleCameraPrivacyButton;
    private ProgressBar mConnectionSpinner;
    private LinearLayout mToolbarLayout;
    private LinearLayout mTutorVideoContainer;
    private RelativeLayout mStudentVideoContainer;

    private TextView mToolbarStatus;
    private FrameLayout mRemote1;
    private FrameLayout mRemote2;
    private FrameLayout mRemote3;
    private FrameLayout mRemote4;
    private FrameLayout mRemote5;
    private FrameLayout mLocalTutor;
    private FrameLayout mLocalStudent;
    private FrameLayout mRemote;
    private FrameLayout mToggleToolbarFrame;
    private boolean mAutoJoin = false;
    private boolean mAllowReconnect = true;
    private VideoChatActivity mSelf;

    private boolean isThisMentor;
    private String vidyo_token;
    private String name;
    private String resourceId;
    private String tId;

    private List<String> rendererSlots = new ArrayList<>();
    private final String OPEN_REMOTE_SLOT = "-1";
    private List<String> remotePeople = new ArrayList<>();
    private VidyoLocalCamera selectedLocalCamera;
    private HashMap<String,RemoteCamera> remoteCameras = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_chat);

        mToggleConnectButton = (ToggleButton) findViewById(R.id.toggleConnectButton);
        mToggleCameraSwitchButton = (ToggleButton) findViewById(R.id.cameraSwitch);
        mToggleCameraPrivacyButton = (ToggleButton) findViewById(R.id.cameraPrivacyButton);
        mToolbarLayout = (LinearLayout) findViewById(R.id.toolbarLayout);
        mToggleToolbarFrame = (FrameLayout) findViewById(R.id.toggleToolbarFrame);
        mTutorVideoContainer = (LinearLayout) findViewById(R.id.tutorVideoContainer);
        mStudentVideoContainer = (RelativeLayout) findViewById(R.id.studentVideoContainer);

        mRemote1 = (FrameLayout) findViewById(R.id.remote1);
        mRemote2 = (FrameLayout) findViewById(R.id.remote2);
        mRemote3 = (FrameLayout) findViewById(R.id.remote3);
        mRemote4 = (FrameLayout) findViewById(R.id.remote4);
        mRemote5 = (FrameLayout) findViewById(R.id.remote5);
        mLocalTutor = (FrameLayout) findViewById(R.id.localTutor);
        mRemote = (FrameLayout) findViewById(R.id.remote);
        mLocalStudent = (FrameLayout) findViewById(R.id.localStudent);

        mToolbarStatus = (TextView) findViewById(R.id.toolbarStatusText);
        mConnectionSpinner = (ProgressBar) findViewById(R.id.connectionSpinner);
        mSelf = this;

        // Suppress keyboard
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        // Initialize the VidyoClient
        Connector.SetApplicationUIContext(this);
        mVidyoClientInitialized = Connector.Initialize();

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        mLeftDrawerLayout=super.mDrawerLayout;
        appPrefs = new AppPrefs(this);
        mAppBar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(mAppBar);

        VideoChatActivity.this.setTitle("Video Tutoring");

        mAppBar.setNavigationIcon(R.drawable.pyoopil_logo_white);
        mAppBar.setNavigationContentDescription("BACK");

        mAppBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLeftDrawerLayout.openDrawer(GravityCompat.START);
            }
        });

        mLeftDrawerLayout.setDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {

            }

            @Override
            public void onDrawerOpened(View drawerView) {
                hideSoftKeyboard();
            }

            @Override
            public void onDrawerClosed(View drawerView) {

            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });

        Intent intent = getIntent();

        if (intent.getExtras() != null) {
            vidyo_token = intent.getStringExtra("vidyo_token");
            isThisMentor = intent.getBooleanExtra("isThisMentor",false);
            name = intent.getStringExtra("name");
            resourceId = intent.getStringExtra("resourceId");
            mPathId = intent.getStringExtra("resourceId");
            tId = intent.getStringExtra("tId")+"@e4a173.vidyo.io";
        }

        if(isThisMentor){
            rendererSlots.add("1");
            rendererSlots.add(OPEN_REMOTE_SLOT);
            rendererSlots.add(OPEN_REMOTE_SLOT);
            rendererSlots.add(OPEN_REMOTE_SLOT);
            rendererSlots.add(OPEN_REMOTE_SLOT);
            rendererSlots.add(OPEN_REMOTE_SLOT);
            showMentor();
            mToggleCameraSwitchButton.setVisibility(View.GONE);
            mToggleCameraPrivacyButton.setVisibility(View.GONE);
        }else{
            showStudent();
        }

    }

    public FrameLayout getRemoteView(final int i){
        if(i==1){
            return mRemote1;
        } else if(i==2){
            return mRemote2;
        }else if(i==3){
            return mRemote3;
        }else if(i==4){
            return mRemote4;
        }else{
            return mRemote5;
        }
    }

    public void showMentor(){
        mTutorVideoContainer.setVisibility(View.VISIBLE);
        mStudentVideoContainer.setVisibility(View.GONE);
    }

    public void showStudent(){
        mStudentVideoContainer.setVisibility(View.VISIBLE);
        mTutorVideoContainer.setVisibility(View.GONE);
    }

    public void exitVideoTutoring(){
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(VideoChatActivity.this);
        alertDialog.setTitle("Exit Video Tutoring?");
        alertDialog.setCancelable(false);
        alertDialog.setMessage("Are you sure you want to exit video tutoring");
        alertDialog.setPositiveButton("Exit",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent dashIntent = new Intent(VideoChatActivity.this, DashBoardActivity.class);
                        dashIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(dashIntent);
                    }
                });
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                dialog.dismiss();
            }
        });
        alertDialog.show();
    }

    @Override
    public void onBackPressed() {
        exitVideoTutoring();
    }

    public void hideSoftKeyboard() {
        if (this.getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_video_chat, menu);

        mMenu = menu;
        final TextView count_textview;
        Firebase fireBaseCountRef;

        final View menu_notification = menu.findItem(R.id.notificationicon).getActionView();
        count_textview = (TextView) menu_notification.findViewById(R.id.tv_unread_count);
        count_textview.setVisibility(View.INVISIBLE);

        Firebase.setAndroidContext(getApplicationContext());
        fireBaseCountRef = new Firebase(ApplicationSingleton.FIREBASE_URL + "users/" + appPrefs.getUser_Id() + "/");

        fireBaseCountRef.child("count").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() == null) {
                    //  Log.e("", "Its empty bro in consume course");
                } else {
                    countNot = Integer.parseInt(dataSnapshot.getValue().toString());
                }
                ApplicationUtility.updateUnreadCount(countNot, VideoChatActivity.this, count_textview);
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });


        menu_notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(VideoChatActivity.this, NotificationActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_from_bottom, R.anim.stay);

                Firebase changeValues = new Firebase(ApplicationSingleton.FIREBASE_URL + "users/" + appPrefs.getUser_Id() + "/");
                changeValues.child("count").setValue(0l);


            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        int id = item.getItemId();

        if (id == R.id.exitVideoChat) { exitVideoTutoring();}

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        Log.e("yash","onResume");
        super.onResume();
        if (mVidyoConnector == null) {

            Log.e("yash","if mVidyoConnector");
            if (mVidyoClientInitialized) {
                Log.e("yash","if mVidyoClientInitialized");
                try {
                    mVidyoConnector = new VidyoConnector(null,
                            VidyoConnector.VidyoConnectorViewStyle.VIDYO_CONNECTORVIEWSTYLE_Default,
                            6,
                            "info@VidyoClient info@VidyoConnector warning",
                            "",
                            0);

                    // Register for local camera callbacks
                    if (!mVidyoConnector.RegisterLocalCameraEventListener(mSelf)) {
                        Log.e("yash","VidyoConnector RegisterLocalCameraEventListener failed");
                    }

                    // Register for remote camera callbacks
                    if (!mVidyoConnector.RegisterRemoteCameraEventListener(mSelf)) {
                        Log.e("yash","VidyoConnector RegisterRemoteCameraEventListener failed");
                    }

                    // Register for local camera callbacks
                    if (!mVidyoConnector.RegisterLocalMicrophoneEventListener(mSelf)) {
                        Log.e("yash","VidyoConnector RegisterLocalMicrophoneEventListener failed");
                    }

                    // Register for local speaker callbacks
                    if (!mVidyoConnector.RegisterLocalSpeakerEventListener(mSelf)) {
                        Log.e("yash","VidyoConnector RegisterLocalSpeakerEventListener failed");
                    }

                    // Register for local speaker callbacks
                    if (!mVidyoConnector.RegisterParticipantEventListener(mSelf)) {
                        Log.e("yash","VidyoConnector RegisterParticipantEventListener failed");
                    }

                    // Register for network interface callbacks
                    if (!mVidyoConnector.RegisterNetworkInterfaceEventListener(mSelf)) {
                        Log.e("yash","VidyoConnector RegisterNetworkInterfaceEventListener failed");
                    }

                    // Register for log callbacks
                    if (!mVidyoConnector.RegisterLogEventListener(mSelf, "info@VidyoClient info@VidyoConnector warning")) {
                        Log.e("yash","VidyoConnector RegisterLogEventListener failed");
                    }
                }
                catch (Exception e) {
                    Log.e("yash","VidyoConnector Construction failed");
                    Log.e("yash","Error VidyoException "+e.getMessage());
                }
            } else {
                Log.e("yash","ERROR: VidyoClientInitialize failed - not constructing VidyoConnector ...");
            }

            Log.e("yash","onResume: mVidyoConnectorConstructed => " + (mVidyoConnector != null ? "success" : "failed"));
        }

        // If configured to auto-join, then simulate a click of the toggle connect button
        if (mAutoJoin && (mVidyoConnector != null)) {
            mToggleConnectButton.performClick();
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (mVidyoConnector != null) {
            mVidyoConnector.SetMode(VidyoConnector.VidyoConnectorMode.VIDYO_CONNECTORMODE_Foreground);
        }
    }

    @Override
    protected void onStop() {
        if (mVidyoConnector != null) {
            mVidyoConnector.SetMode(VidyoConnector.VidyoConnectorMode.VIDYO_CONNECTORMODE_Background);
        }
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        // Release device resources
        mVidyoConnector.Disable();
        mVidyoConnector = null;

        // Uninitialize the VidyoClient library
        Connector.Uninitialize();

        super.onDestroy();
    }

    private void showRenderer(FrameLayout view){
        mVidyoConnector.ShowViewAt(view, 0, 0, view.getWidth(), view.getHeight());
    }

    // Refresh the UI
    private void RefreshUI() {
        // Refresh the rendering of the video
        if(isThisMentor){
            showRenderer(mLocalTutor);
            showRenderer(mRemote1);
            showRenderer(mRemote2);
            showRenderer(mRemote3);
            showRenderer(mRemote4);
            showRenderer(mRemote5);
        }else{
            showRenderer(mLocalStudent);
            showRenderer(mRemote);
        }
    }

    // The state of the VidyoConnector connection changed, reconfigure the UI.
    // If connected, dismiss the controls layout
    private void ConnectorStateUpdated(VIDYO_CONNECTOR_STATE state, final String statusText) {
        Log.e("yash","ConnectorStateUpdated, state = " + state.toString());

        mVidyoConnectorState = state;

        // Execute this code on the main thread since it is updating the UI layout

        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                // Update the toggle connect button to either start call or end call image
                mToggleConnectButton.setChecked(mVidyoConnectorState == VIDYO_CONNECTOR_STATE.VC_CONNECTED);

                // Set the status text in the toolbar
                mToolbarStatus.setText(statusText);

                if (mVidyoConnectorState == VIDYO_CONNECTOR_STATE.VC_CONNECTED) {
                    // Enable the toggle toolbar control
                    mToggleToolbarFrame.setVisibility(View.VISIBLE);

                } else {
                    // VidyoConnector is disconnected

                    // Disable the toggle toolbar control and display toolbar in case it is hidden
                    mToggleToolbarFrame.setVisibility(View.GONE);
                    mToolbarLayout.setVisibility(View.VISIBLE);

                    // If the allow-reconnect flag is set to false and a normal (non-failure) disconnect occurred,
                    // then disable the toggle connect button, in order to prevent reconnection.
                    if (!mAllowReconnect && (mVidyoConnectorState == VIDYO_CONNECTOR_STATE.VC_DISCONNECTED)) {
                        mToggleConnectButton.setEnabled(false);
                        mToolbarStatus.setText("Call ended");
                    }
                }

                // Hide the spinner animation
                mConnectionSpinner.setVisibility(View.INVISIBLE);
            }
        });
    }

    /*
     * Button Event Callbacks
     */

    // The Connect button was pressed.
    // If not in a call, attempt to connect to the backend service.
    // If in a call, disconnect.
    public void ToggleConnectButtonPressed(View v) {
        boolean status;
        if (mToggleConnectButton.isChecked()) {
            mToolbarStatus.setText("Connecting...");

            // Display the spinner animation
            mConnectionSpinner.setVisibility(View.VISIBLE);

            status = mVidyoConnector.Connect("prod.vidyo.io",vidyo_token,name,resourceId,this);


            if (!status) {
                // Hide the spinner animation
                mConnectionSpinner.setVisibility(View.INVISIBLE);

                ConnectorStateUpdated(VIDYO_CONNECTOR_STATE.VC_CONNECTION_FAILURE, "Connection failed");
            }else{
                RefreshUI();
            }

        } else {
            disConnect();
        }
    }

    public void disConnect(){
        // The button just switched to the callStart image: The user is either connected to a resource
        // or is in the process of connecting to a resource; call VidyoConnectorDisconnect to either disconnect
        // or abort the connection attempt.
        // Change the button back to the callEnd image because do not want to assume that the Disconnect
        // call will actually end the call. Need to wait for the callback to be received
        // before swapping to the callStart image.
        remotePeople = null;
        remotePeople = new ArrayList<>();

        mToggleConnectButton.setChecked(true);

        mToolbarStatus.setText("Disconnecting...");

        mVidyoConnector.Disconnect();

        RefreshUI();
    }

    // Toggle the microphone privacy
    public void MicrophonePrivacyButtonPressed(View v) {
        mVidyoConnector.SetMicrophonePrivacy(((ToggleButton) v).isChecked());
    }

    // Toggle the camera privacy
    public void CameraPrivacyButtonPressed(View v) {
        mVidyoConnector.SetCameraPrivacy(((ToggleButton) v).isChecked());
    }

    // Handle the camera swap button being pressed. Cycle the camera.
    public void CameraSwapButtonPressed(View v) {
        mVidyoConnector.CycleCamera();
    }

    // Toggle visibility of the toolbar
    public void ToggleToolbarVisibility(View v) {
        if (mVidyoConnectorState == VIDYO_CONNECTOR_STATE.VC_CONNECTED) {
            if (mToolbarLayout.getVisibility() == View.VISIBLE) {
                mToolbarLayout.setVisibility(View.INVISIBLE);
            } else {
                mToolbarLayout.setVisibility(View.VISIBLE);
            }
        }
    }

    /*
     *  Connector Events
     */

    // Handle successful connection.
    @Override
    public void OnSuccess() {
        ConnectorStateUpdated(VIDYO_CONNECTOR_STATE.VC_CONNECTED, "Connected");
    }

    // Handle attempted connection failure.
    @Override
    public void OnFailure(VidyoConnector.VidyoConnectorFailReason reason) {
        // Update UI to reflect connection failed
        ConnectorStateUpdated(VIDYO_CONNECTOR_STATE.VC_CONNECTION_FAILURE, "Connection failed");
    }

    // Handle an existing session being disconnected.
    @Override
    public void OnDisconnected(VidyoConnector.VidyoConnectorDisconnectReason reason) {
        if (reason == VidyoConnector.VidyoConnectorDisconnectReason.VIDYO_CONNECTORDISCONNECTREASON_Disconnected) {
            ConnectorStateUpdated(VIDYO_CONNECTOR_STATE.VC_DISCONNECTED, "Disconnected");
        } else {
            ConnectorStateUpdated(VIDYO_CONNECTOR_STATE.VC_DISCONNECTED_UNEXPECTED, "Unexpected disconnection");
        }
    }

    // Handle a message being logged.
    @Override
    public void OnLog(VidyoLogRecord logRecord) {
        Log.e("yash",logRecord.message);
    }

    @Override
    public void OnNetworkInterfaceAdded(VidyoNetworkInterface vidyoNetworkInterface) {

    }

    @Override
    public void OnNetworkInterfaceRemoved(VidyoNetworkInterface vidyoNetworkInterface) {
    }

    @Override
    public void OnNetworkInterfaceSelected(VidyoNetworkInterface vidyoNetworkInterface, VidyoNetworkInterface.VidyoNetworkInterfaceTransportType vidyoNetworkInterfaceTransportType) {
    }

    @Override
    public void OnNetworkInterfaceStateUpdated(VidyoNetworkInterface vidyoNetworkInterface, VidyoNetworkInterface.VidyoNetworkInterfaceState vidyoNetworkInterfaceState) {
    }

    //Handle Local Camera Things
    @Override
    public void OnLocalCameraAdded(VidyoLocalCamera vidyoLocalCamera) {
    }

    @Override
    public void OnLocalCameraRemoved(VidyoLocalCamera vidyoLocalCamera) {

    }

    @Override
    public void OnLocalCameraSelected(final VidyoLocalCamera vidyoLocalCamera) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(isThisMentor){
                    if (vidyoLocalCamera != null) {
                        selectedLocalCamera = vidyoLocalCamera;
                        mVidyoConnector.AssignViewToLocalCamera(mLocalTutor,vidyoLocalCamera,true,false);
                        showRenderer(mLocalTutor);
                    } else {
                        mVidyoConnector.HideView(mLocalTutor);
                    }
                }else{
                    if (vidyoLocalCamera != null) {
                        selectedLocalCamera = vidyoLocalCamera;
                        mVidyoConnector.AssignViewToLocalCamera(mLocalStudent,vidyoLocalCamera,true,false);
                        showRenderer(mLocalStudent);
                    } else {
                        mVidyoConnector.HideView(mLocalStudent);
                    }
                }
                Log.e("yash","selected camera "+selectedLocalCamera.toString());
            }
        });
    }

    @Override
    public void OnLocalCameraStateUpdated(VidyoLocalCamera vidyoLocalCamera, VidyoDevice.VidyoDeviceState vidyoDeviceState) {

    }

    //Handle Remote Camera Things
    @Override
    public void OnRemoteCameraAdded(final VidyoRemoteCamera vidyoRemoteCamera, final VidyoParticipant vidyoParticipant) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(isThisMentor){
                    if (vidyoRemoteCamera != null) {
                        RemoteCamera rc = new RemoteCamera();
                        rc.isRendered = false;
                        rc.remoteCamera = vidyoRemoteCamera;
                        remoteCameras.put(vidyoParticipant.GetId(),rc);

                        for(int i=1;i<rendererSlots.size();i++){
                            if(rendererSlots.get(i).equals(OPEN_REMOTE_SLOT)){
                                rendererSlots.set(i,vidyoParticipant.GetId());
                                remoteCameras.get(vidyoParticipant.GetId()).isRendered = true;
                                mVidyoConnector.AssignViewToRemoteCamera(getRemoteView(i),vidyoRemoteCamera,true,false);
                                showRenderer(getRemoteView(i));
                                break;
                            }
                        }
                    }
                }else{
                    if (vidyoRemoteCamera != null) {
                        if(remotePeople.size()==0){
                            remotePeople.add(0,vidyoParticipant.GetUserId());
                            if(remotePeople.get(0).equals(tId)){
                                mVidyoConnector.AssignViewToRemoteCamera(mRemote,vidyoRemoteCamera,true,false);
                                showRenderer(mRemote);
                            }
                        }else{
                            if(!remotePeople.get(0).equals(tId)){
                                disConnect();
                                mToolbarStatus.setText("Tutor unavailable");
                                Toast.makeText(VideoChatActivity.this,"Tutor unavailable", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }
            }
        });
    }

    @Override
    public void OnRemoteCameraRemoved(final VidyoRemoteCamera vidyoRemoteCamera,final VidyoParticipant vidyoParticipant) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(isThisMentor){
                    remoteCameras.remove(vidyoParticipant.GetId());
                    for(int i=1;i<rendererSlots.size();i++){
                        if(rendererSlots.get(i).equals(vidyoParticipant.GetId())){
                            rendererSlots.set(i,OPEN_REMOTE_SLOT);
                            mVidyoConnector.HideView(getRemoteView(i));
                            for(String s : remoteCameras.keySet()){
                                RemoteCamera r = remoteCameras.get(s);
                                if(!r.isRendered){
                                    rendererSlots.set(i,s);
                                    remoteCameras.get(s).isRendered = true;
                                    mVidyoConnector.AssignViewToRemoteCamera(getRemoteView(i),r.remoteCamera,true,false);
                                    showRenderer(getRemoteView(i));
                                    break;
                                }
                            }
                            break;
                        }
                    }
                }else{
                    if(vidyoParticipant.GetUserId().equals(tId)){
                        disConnect();
                        mToolbarStatus.setText("Tutor terminated the session");
                        Toast.makeText(VideoChatActivity.this,"Tutor terminated the session", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    @Override
    public void OnRemoteCameraStateUpdated(VidyoRemoteCamera vidyoRemoteCamera, VidyoParticipant vidyoParticipant, VidyoDevice.VidyoDeviceState vidyoDeviceState) {

    }

    //Handle Microphone Things
    @Override
    public void OnLocalMicrophoneAdded(VidyoLocalMicrophone vidyoLocalMicrophone) {

    }

    @Override
    public void OnLocalMicrophoneRemoved(VidyoLocalMicrophone vidyoLocalMicrophone) {

    }

    @Override
    public void OnLocalMicrophoneSelected(VidyoLocalMicrophone vidyoLocalMicrophone) {

    }

    @Override
    public void OnLocalMicrophoneStateUpdated(VidyoLocalMicrophone vidyoLocalMicrophone, VidyoDevice.VidyoDeviceState vidyoDeviceState) {

    }

    //Handle Speaker Things
    @Override
    public void OnLocalSpeakerAdded(VidyoLocalSpeaker vidyoLocalSpeaker) {
    }

    @Override
    public void OnLocalSpeakerRemoved(VidyoLocalSpeaker vidyoLocalSpeaker) {

    }

    @Override
    public void OnLocalSpeakerSelected(VidyoLocalSpeaker vidyoLocalSpeaker) {
    }

    @Override
    public void OnLocalSpeakerStateUpdated(VidyoLocalSpeaker vidyoLocalSpeaker, VidyoDevice.VidyoDeviceState vidyoDeviceState) {

    }

    //Handle Participant Things
    @Override
    public void OnParticipantJoined(final VidyoParticipant vidyoParticipant) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(isThisMentor){
                    Toast.makeText(VideoChatActivity.this,vidyoParticipant.GetName()+" Joined", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void OnParticipantLeft(final VidyoParticipant vidyoParticipant) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(isThisMentor){
                    Toast.makeText(VideoChatActivity.this,vidyoParticipant.GetName()+" Left", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void OnDynamicParticipantChanged(ArrayList<VidyoParticipant> arrayList, ArrayList<VidyoRemoteCamera> arrayList1) {

    }

    @Override
    public void OnLoudestParticipantChanged(final VidyoParticipant vidyoParticipant, boolean b) {
    }

    class RemoteCamera{
        VidyoRemoteCamera remoteCamera;
        boolean isRendered;

        @Override
        public String toString() {
            return "RemoteCamera{" +
                    "remoteCamera=" + remoteCamera +
                    ", isRendered=" + isRendered +
                    '}';
        }
    }
}
