package in.zapprep.ZapPrep;

/**
 * Created by PoolDead(AKA Naveen) on 11/16/2017.
 */

public class VidyoTokenResponseData {

    private String vidyo_token;

    public String getVidyo_token() {
        return vidyo_token;
    }

    public void setVidyo_token(String vidyo_token) {
        this.vidyo_token = vidyo_token;
    }
}
