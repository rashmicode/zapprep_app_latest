package in.zapprep.ZapPrep;

import java.util.Arrays;

/**
 * Created by PoolDead on 6/30/2018.
 */

public class QuestionData {
    public String id;
    public String name;
    public String choices[];
    public String images[];
    public String mark;
    public String hasMultipleAnswer;
    public String maxAnswer;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String[] getChoices() {
        return choices;
    }

    public void setChoices(String[] choices) {
        this.choices = choices;
    }

    public String[] getImages() {
        return images;
    }

    public void setImages(String[] images) {
        this.images = images;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public String isHasMultipleAnswer() {
        return hasMultipleAnswer;
    }

    public void setHasMultipleAnswer(String hasMultipleAnswer) {
        this.hasMultipleAnswer = hasMultipleAnswer;
    }

    public String getMaxAnswer() {
        return maxAnswer;
    }

    public void setMaxAnswer(String maxAnswer) {
        this.maxAnswer = maxAnswer;
    }

    @Override
    public String toString() {
        return "QuestionData{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", choices=" + Arrays.toString(choices) +
                ", images=" + Arrays.toString(images) +
                ", mark='" + mark + '\'' +
                ", hasMultipleAnswer='" + hasMultipleAnswer + '\'' +
                ", maxAnswer='" + maxAnswer + '\'' +
                '}';
    }
}

