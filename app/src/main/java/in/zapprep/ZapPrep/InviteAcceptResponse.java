package in.zapprep.ZapPrep;

/**
 * Created by PoolDead on 12/30/2017.
 */

public class InviteAcceptResponse {
    boolean isAccepted;

    public boolean isAccepted() {
        return isAccepted;
    }

    public void setAccepted(boolean accepted) {
        isAccepted = accepted;
    }
}
