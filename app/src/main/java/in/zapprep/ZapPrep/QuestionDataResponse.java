package in.zapprep.ZapPrep;

import java.util.List;

/**
 * Created by PoolDead on 7/3/2018.
 */

public class QuestionDataResponse {
    List<QuestionData> data;

    public List<QuestionData> getData() {
        return data;
    }

    public void setData(List<QuestionData> data) {
        this.data = data;
    }
}
