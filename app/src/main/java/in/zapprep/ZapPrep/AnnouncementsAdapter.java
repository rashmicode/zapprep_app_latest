package in.zapprep.ZapPrep;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.bumptech.glide.Glide;
import com.koushikdutta.ion.Ion;

import java.util.List;

/**
 * Created by Tushar on 7/14/2015.
 */
public class AnnouncementsAdapter extends BaseAdapter {

    public List<AnnouncementData> mAnnouncementDataList;
    Context mContext;
    int mGridPosition;
    RelativeLayout tileView;
    String desc;
    int flag=0;
    TextView announcement;
    TextView announcementDetail;
    public AnnouncementsAdapter(Context context, List<AnnouncementData> listAnnouncement, int gridPosition) {
        this.mAnnouncementDataList = listAnnouncement;
        this.mContext = context;
        mGridPosition = gridPosition;
    }

    @Override
    public int getCount() {
        return mAnnouncementDataList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);


       // Log.v("ListAdapter", "Size of lists list" + mAnnouncementDataList);

        View listView = new View(mContext);

        if (convertView == null) {
            listView = inflater.inflate(R.layout.fragment_announcement_content, null);
        } else {
            listView = (View) convertView;
        }

        ImageView mentorPhoto = (ImageView) listView.findViewById(R.id.mentor_photo);
        TextView mentorName = (TextView) listView.findViewById(R.id.userName);
        announcement = (TextView) listView.findViewById(R.id.announcementDesc);
        announcementDetail = (TextView) listView.findViewById(R.id.announcementDescDetail);
        TextView timeStamp = (TextView) listView.findViewById(R.id.timestamp);
        tileView = (RelativeLayout) listView.findViewById(R.id.contentwithBG);
        if (DashBoardActivity.listDash.get(mGridPosition).getProducer() == null) {
            AppPrefs appPrefs = new AppPrefs(mContext);
            Glide.with(mContext)
                    .load(appPrefs.getUser_Avatar())
                    .centerCrop()
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder)
                    .crossFade()
                    .dontAnimate()
                    .into(mentorPhoto);
            mentorName.setText(appPrefs.getUser_FullName());
            mentorName.setTextColor(Color.parseColor("#ef3b24"));
        } else {
            Glide.with(mContext)
                    .load(DashBoardActivity.listDash.get(mGridPosition).getProducer().getAvatar())
                    .centerCrop()
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder)
                    .crossFade()
                    .dontAnimate()
                    .into(mentorPhoto);
            mentorName.setText(DashBoardActivity.listDash.get(mGridPosition).getProducer().getName());
            mentorName.setTextColor(Color.parseColor("#ef3b24"));
        }


        announcement.setText(mAnnouncementDataList.get(position).getDesc());
        desc = mAnnouncementDataList.get(position).getDesc();
        timeStamp.setText(getDateTime(mAnnouncementDataList.get(position).getTimestamp()));
        return listView;
    }


    public void add(AnnouncementData announcementData) {
        mAnnouncementDataList.add(announcementData);
    }

    /**
     * Add to adapter at a particular index
     *
     * @param index   index/position to add to
     * @param announcementData chat message to add
     */
    public void add(int index,AnnouncementData announcementData) {
        mAnnouncementDataList.add(index, announcementData);
    }

    public String getDateTime(String ISODate){
        Date dateString=null;
        SimpleDateFormat dateFormatParse = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss'Z'");
        SimpleDateFormat sdf = new SimpleDateFormat("h:mm a d MMM yyyy");
        String targetDate = ISODate;
        targetDate = targetDate.substring(0,19)+"Z";
        try{
            dateString = dateFormatParse.parse(targetDate);
        }catch (ParseException e){
            Log.e("YashEx",e.toString());
        }
        return sdf.format(dateString);
    }

}
