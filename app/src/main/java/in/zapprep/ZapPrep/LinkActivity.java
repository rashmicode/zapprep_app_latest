package in.zapprep.ZapPrep;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import java.util.List;


public class LinkActivity extends AppCompatActivity {
    String mPathID;
    String mUserId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_link);
        Uri data = getIntent().getData();
        if(data!=null)
        {
            String scheme = data.getScheme(); // "http"
            String host = data.getHost(); // "www.pyoopil.com"
            List<String> params = data.getPathSegments();
            Log.e("","Params size"+params.size());
            if(params.size()==6) {
                String first = params.get(0); // "status"
                String second = params.get(1);
                String third=params.get(2);
                String fourth=params.get(3);
                String fifth=params.get(4);
                String sixth=params.get(5);

                mPathID=fifth;
                Intent intent=new Intent(LinkActivity.this,ExploreCourse.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("pathID",mPathID);
                startActivity(intent);

            }
            else if(params.size()==3)
            {
                String first = params.get(0); // "status"
                String second = params.get(1);
                String third = params.get(2);
                mUserId=second;
                Intent intent=new Intent(LinkActivity.this,ProfilePageActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                //intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("user_id",mUserId);
                startActivity(intent);
            }
            else{
               Intent intent=new Intent(LinkActivity.this,LandingPageActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                //intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
//                LinkActivity.la.finish();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_link, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
