package in.zapprep.ZapPrep;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by PoolDead on 12/22/2017.
 */

public class InvitePageAdapter extends BaseAdapter implements Filterable  {
    Context context;
    int i = 0,invitedCount=0;
    List<InviteData> peopleList;
    List<InviteData> listSecondaryPeople;
    List<Integer> listInvitedPeople;
    String mPathID;

    public InvitePageAdapter(Context context, List<InviteData> peopleList,String mPathID) {
        this.context = context;
        this.peopleList = peopleList;
        this.listSecondaryPeople = peopleList;
        this.mPathID = mPathID;
        listInvitedPeople = new ArrayList<>();
    }

    public View getView(final int position,View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        Typeface tf = Typeface.createFromAsset(context.getAssets(), "Lato-Regular.ttf");

        View listView = new View(context);
        if (convertView == null) {
            listView = inflater.inflate(R.layout.invite_people_list, null);
        } else {
            listView = (View) convertView;
        }

        TextView name = (TextView) listView.findViewById(R.id.nameTextView);
        TextView username = (TextView) listView.findViewById(R.id.usernameTextView);
        ImageView userImage = (ImageView) listView.findViewById(R.id.user_photo);
        RelativeLayout peopleTile = (RelativeLayout) listView.findViewById(R.id.content_frame);
        final SwitchCompat inviteSwitch = (SwitchCompat) listView.findViewById(R.id.inviteSwitch);
        //final Button inviteSwitch = (Button) listView.findViewById(R.id.inviteSwitch);

        if (peopleList.size() != 0) {
            name.setText(peopleList.get(position).getName());
            username.setText( "@" + peopleList.get(position).getUsername());

            if(peopleList.get(position).isInvite()){
                inviteSwitch.setChecked(true);
                if(!listInvitedPeople.contains(position)){
                    listInvitedPeople.add(position);
                }
            }else{
                inviteSwitch.setChecked(false);
            }

            if(!listInvitedPeople.isEmpty()){
                invitedCount = listInvitedPeople.size();
            }

            Glide.with(context)
                    .load(peopleList.get(position).getAvatar())
                    .centerCrop()
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder)
                    .crossFade()
                    .dontAnimate()
                    .into(userImage);

        }

        name.setTypeface(ApplicationSingleton.robotoRegular);
        username.setTypeface(ApplicationSingleton.robotoRegular);

        inviteSwitch.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return event.getActionMasked() == MotionEvent.ACTION_MOVE;
            }
        });

        inviteSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                if (inviteSwitch.isChecked()) {
                    if(invitedCount>=15){
                        inviteSwitch.setChecked(false);
                        Toast.makeText(v.getContext(),"Maximum invitation limit reached. Only 15 people can be invited.",Toast.LENGTH_SHORT).show();
                    }else{
                        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(v.getContext());
                        alertDialog.setTitle("Invite People");
                        alertDialog.setCancelable(false);
                        alertDialog.setMessage("Are you sure you want to invite "+peopleList.get(position).getName()+" ?");
                        alertDialog.setPositiveButton("Invite",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        RestClient.get(v.getContext()).postInvite(peopleList.get(position).getId(), mPathID, new Callback<InviteResponse>() {
                                            @Override
                                            public void success(InviteResponse invite, Response response) {
                                                if(invite.isInvite()){
                                                    inviteSwitch.setChecked(true);
                                                    if(!listInvitedPeople.contains(position)){
                                                        listInvitedPeople.add(position);
                                                        invitedCount++;
                                                    }
                                                    Toast.makeText(v.getContext(),"Request sent to "+peopleList.get(position).getName(),Toast.LENGTH_SHORT).show();
                                                    //notifyDataSetChanged();
                                                }else{
                                                    Log.e("checkSwitch","Invite False "+ listInvitedPeople.size());
                                                    Toast.makeText(v.getContext(),response.toString(),Toast.LENGTH_SHORT).show();
                                                }
                                            }

                                            @Override
                                            public void failure(RetrofitError error) {
                                                inviteSwitch.setChecked(false);
                                                Log.e("checkSwitch","Some error occured");
                                            }
                                        });
                                    }
                                });
                        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                                dialog.dismiss();
                                inviteSwitch.setChecked(false);
                            }
                        });
                        alertDialog.show();
                    }
                } else {
                    final AlertDialog.Builder alertDialog = new AlertDialog.Builder(v.getContext());
                    alertDialog.setTitle("Cancel Invitation");
                    alertDialog.setCancelable(false);
                    alertDialog.setMessage("Are you sure you want to cancel the invite sent to "+peopleList.get(position).getName()+" ?");
                    alertDialog.setPositiveButton("Yes",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {

                                    RestClient.get(v.getContext()).postCancelInvite(peopleList.get(position).getId(), mPathID, new Callback<InviteResponse>() {
                                        @Override
                                        public void success(InviteResponse invite, Response response) {
                                            int removeIndex = -1;
                                            if(!invite.isInvite()){
                                                inviteSwitch.setChecked(false);
                                                removeIndex = listInvitedPeople.indexOf(position);
                                                if(removeIndex!=-1){
                                                    listInvitedPeople.remove(removeIndex);
                                                    invitedCount--;
                                                }
                                                Toast.makeText(v.getContext(),"Cancelled invitation",Toast.LENGTH_SHORT).show();
                                                //notifyDataSetChanged();
                                            }else{
                                                Log.e("checkSwitch","Cancel Invite True"+ listInvitedPeople.size());
                                                Toast.makeText(v.getContext(),response.toString(),Toast.LENGTH_SHORT).show();
                                            }
                                        }

                                        @Override
                                        public void failure(RetrofitError error) {
                                            inviteSwitch.setChecked(true);
                                            Log.e("checkSwitch","Some error occured");
                                        }
                                    });

                                }
                            });
                    alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                            dialog.dismiss();
                            inviteSwitch.setChecked(true);
                        }
                    });
                    alertDialog.show();
                }

            }
        });

        return listView;
    }

    @Override
    public int getCount() {

        if (peopleList == null) {
            return 0;
        } else {
            return peopleList.size();
        }
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected Filter.FilterResults performFiltering(CharSequence charSequence) {
                peopleList = listSecondaryPeople;
                FilterResults results = new FilterResults();

                if (charSequence == null || charSequence.length() == 0) {
                    // No filter implemented we return all the list
                    peopleList = listSecondaryPeople;
                    results.values = peopleList;
                    results.count = peopleList.size();
                    Log.e("performFiltering ", "char sequence null");

                } else {
                    Log.e("performFiltering ", "char sequence : " + charSequence);

                    // We perform filtering operation
                    List<InviteData> nFilteredList = new ArrayList<InviteData>();

                    for (InviteData peopleData : peopleList) {
                        if ((peopleData.getName().toUpperCase().contains(charSequence.toString().toUpperCase())) || (peopleData.getUsername().toUpperCase().contains(charSequence.toString().toUpperCase()))
                                )
                            nFilteredList.add(peopleData);
                    }

                    results.values = nFilteredList;
                    results.count = nFilteredList.size();

                }
                return results;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                if (filterResults.count == 0) {
                    ((InviteActivity) context ).mNomatch.setVisibility(View.VISIBLE);
                    peopleList = (List<InviteData>) filterResults.values;
                    notifyDataSetChanged();
                    Log.e("publishResults ", "char sequence : " + charSequence);
                } else {
                    ((InviteActivity) context).mNomatch.setVisibility(View.GONE);
                    Log.e("publishResults ", "char sequence : " + charSequence);
                    peopleList = (List<InviteData>) filterResults.values;
                    notifyDataSetChanged();
                }
            }
        };
    }

}
