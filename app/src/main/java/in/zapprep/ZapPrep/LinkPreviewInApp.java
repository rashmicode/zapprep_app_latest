package in.zapprep.ZapPrep;

import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;

public class LinkPreviewInApp extends AppCompatActivity {

    String link;
    String title;
    WebView mWebView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_link_preview_in_app);
        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        toolbar.setNavigationContentDescription("BACK");

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mWebView=(WebView)findViewById(R.id.web_view_link);

       if(getIntent().getExtras()!=null)
       {
           link=getIntent().getStringExtra("link");
           title=getIntent().getStringExtra("title");
           startWebView(link);
       }

        LinkPreviewInApp.this.setTitle(title);

    }
    // @Override
    // public boolean onCreateOptionsMenu(Menu menu) {

    //     MenuInflater menuInflater = getMenuInflater();
    //     menuInflater.inflate(R.menu.menu_settings, menu);


    //     return super.onCreateOptionsMenu(menu);
    // }

    // @Override
    // public boolean onOptionsItemSelected(MenuItem item) {

    //     switch (item.getItemId()) {
    //         case R.id.action_browser:
    //             try{
    //             Intent intent = new Intent();
    //             intent.setAction(Intent.ACTION_VIEW);
    //             intent.addCategory(Intent.CATEGORY_BROWSABLE);
    //             intent.addCategory(Intent.CATEGORY_DEFAULT);
    //             intent.setDataAndType(Uri.parse(link), "text/html");
    //             startActivity(intent);
    //             //startActivity(Intent.createChooser(intent, "No Apps Found"));
    //             }
    //             catch(Exception e){
    //                 Intent intent = new Intent();
    //                 intent.setAction(Intent.ACTION_VIEW);
    //                 intent.addCategory(Intent.CATEGORY_BROWSABLE);
    //                 intent.addCategory(Intent.CATEGORY_DEFAULT);
    //                 intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    //                 intent.setComponent(new ComponentName("com.android.browser", "com.android.browser.BrowserActivity"));
    //                 intent.setDataAndType(Uri.parse(link), "text/html");
    //                 startActivity(intent);
    //                // Log.e("LinkPreview","Error is:"+e);
    //                 Crashlytics.logException(e);
    //             }
    //             break;
    //     }

    //     return super.onOptionsItemSelected(item);
    // }

    private void startWebView(String url) {

        //Create new webview Client to show progress dialog
        //When opening a url or click on link

        mWebView.setWebViewClient(new WebViewClient() {

            ProgressDialog progressDialog;

            //YashDev's Code
           @Override
           public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
               String summary = "<html><body><h1 style='text-align:center;padding-top:10px;font-size:14px;font-weight:bold;color:#110000;'>Temporarily Unavailable, Please visit later.</h1></body></html>";
               view.loadData(summary, "text/html", null);
               Toast.makeText(getApplicationContext(), "Temporarily Unavailable, Please visit later.", Toast.LENGTH_SHORT).show();
           }

            //If you will not use this method url links are opeen in new brower not in webview
//            public boolean shouldOverrideUrlLoading(WebView view, String url) {
//                view.loadUrl(url);
//                return true;
//            }

            //Show loader on url load

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                if (progressDialog == null) {
                    // in standard case YourActivity.this
                    progressDialog = new ProgressDialog(LinkPreviewInApp.this);
                    progressDialog.setTitle("Please Wait");
                    progressDialog.setMessage("Loading...");
                    progressDialog.show();
                }
                super.onPageStarted(view, url, favicon);
            }

            public void onPageFinished(WebView view, String url) {
                try{
                    if (progressDialog.isShowing()) {
                        progressDialog.dismiss();
                        progressDialog = null;
                    }
                }catch(Exception exception){
                    exception.printStackTrace();
                }
            }

        });

        // Javascript inabled on webview
        mWebView.getSettings().setJavaScriptEnabled(true);

        // Other webview options

        mWebView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        mWebView.getSettings().setLoadWithOverviewMode(true);
        mWebView.getSettings().setUseWideViewPort(true);

        mWebView.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
        mWebView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        mWebView.getSettings().setDomStorageEnabled(true);
        mWebView.getSettings().setDatabaseEnabled(true);
        mWebView.getSettings().setAppCacheEnabled(true);
        mWebView.getSettings().setSupportZoom(false);
        mWebView.getSettings().setBuiltInZoomControls(false);
        mWebView.getSettings().setDisplayZoomControls(false);
        mWebView.getSettings().setDefaultZoom(WebSettings.ZoomDensity.MEDIUM);

        //Load url in webview
        mWebView.loadUrl(url);

    }

    @Override
    // Detect when the back button is pressed
    public void onBackPressed() {
        if(mWebView.canGoBack()) {
            mWebView.goBack();
        } else {
            // Let the system handle the back button
            super.onBackPressed();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if(keyCode==KeyEvent.KEYCODE_BACK && mWebView.canGoBack()){
            onBackPressed();
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }
}
