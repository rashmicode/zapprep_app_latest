package in.zapprep.ZapPrep;

/**
 * Created by Tushar on 6/13/2015.
 */
public class DashBoardProducer {

    public String username;
    public String email;
    public String avatar;
    public String qbId;
    public String name;
    public String _id;
    public String tagline;
    public boolean onlineStatus;

    public boolean isOnlineStatus() {
        return onlineStatus;
    }

    public void setOnlineStatus(boolean onlineStatus) {
        this.onlineStatus = onlineStatus;
    }

    public String getTagline() {
        return tagline;
    }

    public void setTagline(String tagline) {
        this.tagline = tagline;
    }

    public String getId() {
        return _id;
    }

    public void setId(String _id) {
        this._id = _id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getQbId() {
        return qbId;
    }

    public void setQbId(String qbId) {
        this.qbId = qbId;
    }

    @Override
    public String toString() {
        return "DashBoardProducer{" +
                "username='" + username + '\'' +
                ", email='" + email + '\'' +
                ", avatar='" + avatar + '\'' +
                ", qbId='" + qbId + '\'' +
                ", name='" + name + '\'' +
                ", _id='" + _id + '\'' +
                ", tagline='" + tagline + '\'' +
                ", onlineStatus=" + onlineStatus +
                '}';
    }
}
