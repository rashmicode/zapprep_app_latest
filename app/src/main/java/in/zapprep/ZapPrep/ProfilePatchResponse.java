package in.zapprep.ZapPrep;

/**
 * Created by Tushar on 7/28/2015.
 */
public class ProfilePatchResponse {

    private ProfilePatchData data;

    public ProfilePatchData getData() {
        return data;
    }

    public void setData(ProfilePatchData data) {
        this.data = data;
    }
}
