package in.zapprep.ZapPrep;

/**
 * Created by Dell on 2/20/2016.
 */
public class CategoryImagePair {

    public String cat;
    public String thumb;

    public String getCat() {
        return cat;
    }

    public void setCat(String cat) {
        this.cat = cat;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }
}
