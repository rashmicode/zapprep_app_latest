package in.zapprep.ZapPrep;

import android.app.ProgressDialog;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MentorTrasactionFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MentorTrasactionFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MentorTrasactionFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    ExpandableListView mentorTransactionList;
    TextView noTransaction;
    TransactionDetailsAdapter transactionDetailsAdapter;
    List<TransactionData> transactionData;
    CardView transactionCard;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MentorTrasactionFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MentorTrasactionFragment newInstance(String param1, String param2) {
        MentorTrasactionFragment fragment = new MentorTrasactionFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public static MentorTrasactionFragment newInstance() {
        MentorTrasactionFragment fragment = new MentorTrasactionFragment();
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_mentor_trasaction, container, false);

        mentorTransactionList=(ExpandableListView)view.findViewById(R.id.mentor_transaction_list);
        transactionDetailsAdapter=new TransactionDetailsAdapter(getActivity(),new ArrayList<TransactionData>());
        noTransaction=(TextView)view.findViewById(R.id.flagTransaction);
        transactionCard = (CardView) view.findViewById(R.id.transactionCard);

        final ProgressDialog dialog = CustomProgressDialog.getCustomProgressDialog(getActivity());
        dialog.show();
        RestClient.get(getActivity()).getMentorTransactionHistory(new AppPrefs(getActivity()).getAuthCode(), new Callback<TransactionResponse>() {
            @Override
            public void success(TransactionResponse mentorTransactionResponse, Response response) {
                dialog.hide();
                dialog.dismiss();
                transactionData = mentorTransactionResponse.getData();
                if(transactionData.size()==0)
                {
//                    mentorTransactionList.setVisibility(View.GONE);
                    transactionCard.setVisibility(View.GONE);
                    noTransaction.setVisibility(View.VISIBLE);
                }
                Log.e("", "the marketTransactiondara" + transactionData.size());
                transactionDetailsAdapter = new TransactionDetailsAdapter(getActivity(), transactionData);
                setClickListener();
                mentorTransactionList.setAdapter(transactionDetailsAdapter);
            }

            @Override
            public void failure(RetrofitError error) {
                dialog.hide();
                dialog.dismiss();
                GiveErrorSortIt.SortError(error, getActivity());
            }
        });

        return view;
    }
    public void setClickListener() {

        mentorTransactionList.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {
            @Override
            public void onGroupCollapse(int groupPosition) {
                View groupview = (View) transactionDetailsAdapter.getGroupView(groupPosition, true, null, null);
                ImageView imgArrow = (ImageView) groupview.findViewById(R.id.imgArrow);
                imgArrow.setImageResource(R.drawable.ic_down);
                imgArrow.invalidate();
                transactionDetailsAdapter.notifyDataSetChanged();
            }
        });

        mentorTransactionList.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {

                View groupview = (View) transactionDetailsAdapter.getGroupView(groupPosition, true, null, null);
                ImageView imgArrow = (ImageView) groupview.findViewById(R.id.imgArrow);
                imgArrow.invalidate();
                transactionDetailsAdapter.notifyDataSetChanged();
            }
        });
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

}
