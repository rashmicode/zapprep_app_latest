package in.zapprep.ZapPrep;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class InviteActivity extends NavigationActivity {

    Toolbar appBar;
    DrawerLayout mLeftDrawerLayout;
    String mPathId;
    TextView mNomatch;
    AppPrefs appPrefs;
    ListView listView;
    String page;
    InvitePageAdapter invitePageAdapter;
    public List<InviteData> list;
    ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite);
        appBar = (Toolbar) findViewById(R.id.app_bar);
        appPrefs = new AppPrefs(this);
        setSupportActionBar(appBar);
        mLeftDrawerLayout = (DrawerLayout) mfullLayout.findViewById(R.id.drawer_layout);
        mNomatch = (TextView) findViewById(R.id.noMatch);
        appBar.setNavigationIcon(R.drawable.pyoopil_logo_white);
        appBar.setNavigationContentDescription("BACK");

        page="1";

        InviteActivity.this.setTitle("Invite People");

        appBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLeftDrawerLayout.openDrawer(GravityCompat.START);
                ApplicationSingleton.getInstance().hideSoftKeyboard(InviteActivity.this);
            }
        });

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        if(getIntent().getExtras()!=null) {
            mPathId= getIntent().getExtras().getString("pathID");
        }

        listView =(ListView) findViewById(R.id.people_invite_list);

        list = new ArrayList<>();
        invitePageAdapter = new InvitePageAdapter(InviteActivity.this, list , mPathId);
        listView.setAdapter(invitePageAdapter);
        dialog = CustomProgressDialog.getCustomProgressDialog(InviteActivity.this);
        dialog.show();
        getValuesFromBackend();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_people_page, menu);

        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView =
                (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));

        SearchView.OnQueryTextListener textChangeListener = new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextChange(String newText) {
                // this is your adapter that will be filtered
                invitePageAdapter.getFilter().filter(newText);
                Log.e("on text chnge text: ", "" + newText);
                return false;
            }

            @Override
            public boolean onQueryTextSubmit(String query) {
                // this is your adapter that will be filtered
                Log.e("on query submit: ", "" + query);
                invitePageAdapter.getFilter().filter(query);

                return true;
            }
        };
        searchView.setOnQueryTextListener(textChangeListener);
        return super.onCreateOptionsMenu(menu);
    }

//    @Override
//    public void onBackPressed() {
//        Intent intent = new Intent(InviteActivity.this, MentorPreInviteActivity.class);
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        intent.putExtra("pathID", mPathId);
//        startActivity(intent);
//    }

    public void getValuesFromBackend() {
        RestClient.get(this).getInviteData(page, mPathId, new Callback<InvitePageResponse>() {
            @Override
            public void success(InvitePageResponse invitePageResponse, Response response) {
                Log.i("", "SuccessMessage" + invitePageResponse.toString());
                // success!

                list.addAll(invitePageResponse.getData());
                invitePageAdapter.notifyDataSetChanged();

                dialog.hide();
                dialog.dismiss();
            }

            @Override
            public void failure(RetrofitError error) {
                dialog.hide();
                dialog.dismiss();
                GiveErrorSortIt.SortError(error, InviteActivity.this);
                Toast.makeText(InviteActivity.this,"Some error occured",Toast.LENGTH_SHORT).show();
            }
        });
    }



}
