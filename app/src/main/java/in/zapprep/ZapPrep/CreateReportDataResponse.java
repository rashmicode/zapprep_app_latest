package in.zapprep.ZapPrep;

/**
 * Created by PoolDead on 7/6/2018.
 */

public class CreateReportDataResponse {
    CreateReportData data;

    public CreateReportData getData() {
        return data;
    }

    public void setData(CreateReportData data) {
        this.data = data;
    }

}
