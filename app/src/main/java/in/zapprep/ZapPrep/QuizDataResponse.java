package in.zapprep.ZapPrep;

/**
 * Created by PoolDead on 7/3/2018.
 */

public class QuizDataResponse {
    QuizData data;

    public QuizData getData() {
        return data;
    }

    public void setData(QuizData data) {
        this.data = data;
    }
}
