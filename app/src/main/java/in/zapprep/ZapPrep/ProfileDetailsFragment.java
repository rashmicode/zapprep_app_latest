package in.zapprep.ZapPrep;

import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.media.Image;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.koushikdutta.ion.Ion;
import in.zapprep.ZapPrep.Notifications.NotificationActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link ProfileDetailsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProfileDetailsFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER


    RelativeLayout profileContainer;
    TextView userName;
    ImageView userPhoto;
    TextView userDesc;
    TextView tagLine;
    TextView fullName;
    TextView descHeading;
    ProfileData profileData = ProfilePageActivity.profileData;
    int countNot = 0;
    ProgressBar progressBar;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     * <p/>
     * No Params
     *
     * @return A new instance of fragment ProfileDetailsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ProfileDetailsFragment newInstance(int count) {
        ProfileDetailsFragment fragment = new ProfileDetailsFragment();
        Bundle args = new Bundle();
        args.putInt("count", count);
        //args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public ProfileDetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            countNot = getArguments().getInt("count");
            // mParam2 = getArguments().getString(ARG_PARAM2);
        }
        //countNot=
        setHasOptionsMenu(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View profileDetailsView = inflater.inflate(R.layout.fragment_profile_details, container, false);
        profileContainer = (RelativeLayout) profileDetailsView.findViewById(R.id.profilecontainer);
        userPhoto = (ImageView) profileDetailsView.findViewById(R.id.user_imageP);
        userName = (TextView) profileDetailsView.findViewById(R.id.user_name);
        userDesc = (TextView) profileDetailsView.findViewById(R.id.user_desc);
        tagLine = (TextView) profileDetailsView.findViewById(R.id.user_TagLine);
        fullName = (TextView) profileDetailsView.findViewById(R.id.full_name);
        progressBar = (ProgressBar) profileDetailsView.findViewById(R.id.progressBarProfile);
        userName.setTypeface(ApplicationSingleton.robotoRegular);
        userDesc.setTypeface(ApplicationSingleton.robotoRegular);
        tagLine.setTypeface(ApplicationSingleton.robotoRegular);
        fullName.setTypeface(ApplicationSingleton.robotoRegular);
        // countNot=NavigationActivity.countNot;

        fullName.setText(profileData.getName());
        userName.setText("@" + profileData.getUsername());

        if (profileData.getTagline() != null) {
            tagLine.setText(profileData.getTagline());
        } else {
            profileData.setTagline(" ");
        }

        if (profileData.getUserDesc() == null) {
            profileData.setUserDesc(" ");

        } else {
            userDesc.setText(profileData.getUserDesc());
        }

        if (!profileData.getAvatar().contains("https://s3-ap-southeast-1.amazonaws.com/pyoopil-files/default_profile_photo-1.png")) {
            ImageGetter task = new ImageGetter(profileData.getAvatar(), userPhoto, progressBar);
            task.execute();
        } else {
            String fileDefaultImage = "default_image";
            try {
                userPhoto.setImageDrawable(NavigationActivity.getAssetImage(getActivity(), fileDefaultImage));
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

      /*  Glide.with(getActivity())
                .load(profileData.getAvatar())
                .placeholder(R.drawable.loading)
                .error(R.drawable.exclamation_error)
                .centerCrop()
                .dontAnimate()
                .into(userPhoto);*/
       /* Ion.with(userPhoto)

                .error(R.drawable.exclamation_error)
               // .load(profileData.getAvatar());
                //.placeholder(R.drawable.loading)
        .load("https://graph.facebook.com/10206602006322787/picture?type=large");*/

        return profileDetailsView;
    }

   /* // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
*/

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     * /*
     *//*
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }*/
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        MenuInflater menuInflater = getActivity().getMenuInflater();
        menuInflater.inflate(R.menu.menu_edit_profile, menu);
        final View edit_icon = menu.findItem(R.id.editicon).getActionView();
        if (!ProfilePageActivity.appPrefs.getUser_Id().equals(profileData.getId())) {
            menu.findItem(R.id.editicon).setVisible(false);
        }

        /*edit_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ProfileImageCropActivity.class);
                intent.putExtra("profilePhoto", profileData.getAvatar());
                intent.putExtra("userDesc", profileData.getUserDesc());
                intent.putExtra("userTagLine", profileData.getTagline());
                // intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                startActivity(intent);
            }
        });*/

        final TextView count_textview;
        Firebase fireBaseCountRef;
        final View menu_notification = menu.findItem(R.id.notificationicon1).getActionView();
        count_textview = (TextView) menu_notification.findViewById(R.id.tv_unread_count);

        count_textview.setVisibility(View.INVISIBLE);

        Firebase.setAndroidContext(getActivity());
        fireBaseCountRef = new Firebase(ApplicationSingleton.FIREBASE_URL + "users/" + ProfilePageActivity.appPrefs.getUser_Id() + "/");

        fireBaseCountRef.child("count").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


                if (dataSnapshot.getValue() == null) {
                    //Log.e("", "Its empty bro");
                } else {
                    countNot = Integer.parseInt(dataSnapshot.getValue().toString());
                }
                ApplicationUtility.updateUnreadCount(countNot, getActivity(), count_textview);
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });


        menu_notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), NotificationActivity.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.slide_in_from_bottom, R.anim.stay);

                Firebase changeValues = new Firebase(ApplicationSingleton.FIREBASE_URL + "users/" + ProfilePageActivity.appPrefs.getUser_Id() + "/");
                changeValues.child("count").setValue(0l);


            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.editicon) {
            Intent intent = new Intent(getActivity(), ProfileImageCropActivity.class);
            intent.putExtra("profilePhoto", profileData.getAvatar());
            intent.putExtra("userDesc", profileData.getUserDesc());
            intent.putExtra("userTagLine", profileData.getTagline());
            // intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
            startActivity(intent);
        }
//        if (id == R.id.shareButtonProfile) {
//            String title;
//            String url;
//            if (profileData.getName().equals(new AppPrefs(getActivity()).getUser_FullName())) {
//                url = "http://www.pyoopil.com/profile/" + new AppPrefs(getActivity()).getUser_Id() + "/" + new AppPrefs(getActivity()).getUser_Name();
//            } else {
//                url = "http://www.pyoopil.com/profile/" + ((ProfilePageActivity) getActivity()).userId + "/" + profileData.getUsername();
//            }
//            String appUrl = "https://play.google.com/store/apps/details?id=in.zapprep.ZapPrep";
//            String replacedUrl = url.replaceAll(" ", "-");
//            Log.e("Profile Avtivity", "Share button clicked");
//            Intent i = new Intent(android.content.Intent.ACTION_SEND);
//            i.setType("text/plain");
//            i.putExtra(android.content.Intent.EXTRA_TEXT, "Hey! I am following this person on Pyoopil. Download the app from this link -\n" + appUrl + "\n\nand enroll into his courses - \n" + replacedUrl);
//            startActivity(Intent.createChooser(i, "Share via"));
//        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        // Make sure that we are currently visible
        if (this.isVisible()) {
            // If we are becoming invisible, then...
            ProfilePageActivity.mNomatch.setVisibility(View.GONE);
        }
    }

}
