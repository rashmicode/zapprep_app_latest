package in.zapprep.ZapPrep;

/**
 * Created by Tushar on 7/17/2015.
 */
public class StarredChatSenderData {

    public StarredSenderData sender;
    public String userId;
    public String roomId;
    public String roomName;
    public String messageId;
    public String timestamp;

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public StarredSenderData getSender() {
        return sender;
    }

    public void setSender(StarredSenderData sender) {
        this.sender = sender;
    }
}
