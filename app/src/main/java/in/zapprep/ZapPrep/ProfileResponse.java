package in.zapprep.ZapPrep;

/**
 * Created by Tushar on 6/11/2015.
 */
public class ProfileResponse {
   ProfileData data;
    String more;
    String error;

    public ProfileData getData() {
        return data;
    }

    public void setData(ProfileData data) {
        this.data = data;
    }

    public String getMore() {
        return more;
    }

    public void setMore(String more) {
        this.more = more;
    }
}
