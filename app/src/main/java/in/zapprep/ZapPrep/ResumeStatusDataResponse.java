package in.zapprep.ZapPrep;

/**
 * Created by PoolDead on 7/3/2018.
 */

public class ResumeStatusDataResponse {
    ResumeStatusData data;

    public ResumeStatusData getData() {
        return data;
    }

    public void setData(ResumeStatusData data) {
        this.data = data;
    }
}
