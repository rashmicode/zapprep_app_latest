package in.zapprep.ZapPrep;

import java.io.Serializable;
import java.util.List;
import javax.xml.validation.Schema;

/**
 * Created by Tushar on 5/28/2015.
 */
public class ConceptObjects implements Serializable{

    public String mimetype;
    public String id;
    public String videoid;
    public String type;
    public String url;
    public String title;
    public String desc;
    public String image;
    public String urlThumb;
    public String duration;
    public String negativeMark;
    public boolean quizType;
    public String practiceCount;
    public String questions[];

    public String getMimetype() {
        return mimetype;
    }

    public void setMimetype(String mimetype) {
        this.mimetype = mimetype;
    }

    public String getVideoid() {
        return videoid;
    }

    public void setVideoid(String videoid) {
        this.videoid = videoid;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getNegativeMark() {
        return negativeMark;
    }

    public void setNegativeMark(String negativeMark) {
        this.negativeMark = negativeMark;
    }

    public boolean isQuizType() {
        return quizType;
    }

    public void setQuizType(boolean quizType) {
        this.quizType = quizType;
    }

    public String getPracticeCount() {
        return practiceCount;
    }

    public void setPracticeCount(String practiceCount) {
        this.practiceCount = practiceCount;
    }

    public String[] getQuestions() {
        return questions;
    }

    public void setQuestions(String[] questions) {
        this.questions = questions;
    }

    public String getVideoID() {
        return videoid;
    }

    public void setVideoID(String videoid) {
        this.videoid = videoid;
    }

    public String getUrlThumb() {
        return urlThumb;
    }

    public void setUrlThumb(String urlThumb) {
        this.urlThumb = urlThumb;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
