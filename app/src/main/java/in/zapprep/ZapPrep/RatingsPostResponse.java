package in.zapprep.ZapPrep;

/**
 * Created by tusharbanka on 9/17/2015.
 */
public class RatingsPostResponse {

    RatingsData data;

    public RatingsData getData() {
        return data;
    }

    public void setData(RatingsData data) {
        this.data = data;
    }
}
