package in.zapprep.ZapPrep;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import in.zapprep.ZapPrep.Notifications.NotificationActivity;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MentorPreInviteActivity extends NavigationActivity {

    DrawerLayout mLeftDrawerLayout;
    static String mPathId;
    private Toolbar mAppBar;
    private AppPrefs appPrefs;
    ProgressDialog dialog;
    Menu mMenu;
    public List<InviteData> invitelist,extraInvitelist;
    Button videoJoinBtn,inviteBtn;
    boolean acceptInvite = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mentor_pre_invite);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        mLeftDrawerLayout=super.mDrawerLayout;
        appPrefs = new AppPrefs(this);
        invitelist = new ArrayList<>();
        extraInvitelist = new ArrayList<>();
        mAppBar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(mAppBar);

        MentorPreInviteActivity.this.setTitle("Video Tutoring");

        mAppBar.setNavigationIcon(R.drawable.pyoopil_logo_white);
        mAppBar.setNavigationContentDescription("BACK");

        mAppBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLeftDrawerLayout.openDrawer(GravityCompat.START);
            }
        });

        mLeftDrawerLayout.setDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {

            }

            @Override
            public void onDrawerOpened(View drawerView) {
                hideSoftKeyboard();
            }

            @Override
            public void onDrawerClosed(View drawerView) {

            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });

        if (getIntent().getExtras() != null) {
            mPathId = getIntent().getExtras().getString("pathID");
            if (getIntent().getExtras().getString("redirectNoti") != null && getIntent().getExtras().getString("redirectNoti").equals("AcceptInvite")) {
                acceptInvite = true;
            }
        }

        //Takes to Video Chat Page
        videoJoinBtn = (Button) findViewById(R.id.videoJoinBtn);
        //Takes you to Invite Page
        inviteBtn = (Button) findViewById(R.id.inviteBtn);

        final boolean isMentor = DashBoardActivity.listDash.get(ApplicationUtility.getListPositionFromPathID(mPathId, "redirectDashBoard")).getIsOwner();

        if(!isMentor){
            inviteBtn.setVisibility(View.GONE);
        }else{
            inviteBtn.setVisibility(View.VISIBLE);
        }

        if(acceptInvite){
            showDialogToAcceptInvitation();
        }

        videoJoinBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               if(isMentor){
                   checkInviteAcceptedGoToVideoChat();
                }else{
                   //Toast.makeText(ConsumeCoursePage.this, "Please hold your horses", Toast.LENGTH_SHORT).show();
                     checkIfStudentAcceptedInvite();
                }
            }
        });

        inviteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        goToInvitePage();
                    }
                }, 350);
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_pre_invite_page, menu);

        mMenu = menu;
        final TextView count_textview;
        Firebase fireBaseCountRef;

        final View menu_notification = menu.findItem(R.id.notificationicon).getActionView();
        count_textview = (TextView) menu_notification.findViewById(R.id.tv_unread_count);
        count_textview.setVisibility(View.INVISIBLE);

        Firebase.setAndroidContext(getApplicationContext());
        fireBaseCountRef = new Firebase(ApplicationSingleton.FIREBASE_URL + "users/" + appPrefs.getUser_Id() + "/");

        fireBaseCountRef.child("count").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() == null) {
                    //  Log.e("", "Its empty bro in consume course");
                } else {
                    countNot = Integer.parseInt(dataSnapshot.getValue().toString());
                }
                ApplicationUtility.updateUnreadCount(countNot, MentorPreInviteActivity.this, count_textview);
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });


        menu_notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MentorPreInviteActivity.this, NotificationActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_from_bottom, R.anim.stay);

                Firebase changeValues = new Firebase(ApplicationSingleton.FIREBASE_URL + "users/" + appPrefs.getUser_Id() + "/");
                changeValues.child("count").setValue(0l);


            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        int id = item.getItemId();

        if (id == R.id.exitPreInvitePage) {
//            Intent dashIntent = new Intent(MentorPreInviteActivity.this, DashBoardActivity.class);
//            dashIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            startActivity(dashIntent);
            this.onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    public void hideSoftKeyboard() {
        if (this.getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
        }
    }

    public void checkIfStudentAcceptedInvite(){

        RestClient.get(MentorPreInviteActivity.this).getInviteData("1", mPathId, new Callback<InvitePageResponse>() {
            int count=0;
            @Override
            public void success(InvitePageResponse invitePageResponse, Response response) {

                Log.i("flaggy", "SuccessMessage" + invitePageResponse.toString());
                // success!

                invitelist.addAll(invitePageResponse.getData());

                if(!invitelist.isEmpty()){
                    for (InviteData inviteData:invitelist) {
                        if(inviteData.getUsername().equals(appPrefs.getUser_Name())){
                            if(inviteData.isInvite()){
                                if(inviteData.isAccepted()){
                                    count=1;
                                }
                            }else{
                                count=2;
                            }
                        }
                    }
                    goToVideoActivity(count,"student");
                }

            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("flaggy", "SuccessMessage" + error.toString());
                GiveErrorSortIt.SortError(error, MentorPreInviteActivity.this);
            }
        });
    }

    public void goToVideoActivity(final int count,String source){
        if(source.equals("mentor")){
            if(count==0){
                Toast.makeText(MentorPreInviteActivity.this,"No people have accepted the invitation. Please come back later.",Toast.LENGTH_SHORT).show();
            }else{
                redirectToVideoChatPage();
            }
        }else if(source.equals("student")){
            if(count == 0){
                showDialogToAcceptInvitation();
            }else if(count == 1){
                redirectToVideoChatPage();
            }else if(count == 2){
                Toast.makeText(MentorPreInviteActivity.this,"You have not yet received the invitation to join the video tutoring from mentor. Please check later.",Toast.LENGTH_LONG).show();
            }else if (count==3){
                Toast.makeText(MentorPreInviteActivity.this,"Invitation Expired. Please request mentor to send request again.",Toast.LENGTH_LONG).show();
            }
        }
    }

    public void showDialogToAcceptInvitation(){
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(MentorPreInviteActivity.this);
        alertDialog.setTitle("Accept Invitation");
        alertDialog.setCancelable(false);
        alertDialog.setMessage("Please accept the invitation to use Video Tutoring for this Course");
        alertDialog.setPositiveButton("Accept",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        RestClient.get(MentorPreInviteActivity.this).postAcceptInvite(appPrefs.getUser_Id(), mPathId, new Callback<InviteAcceptResponse>() {
                            @Override
                            public void success(InviteAcceptResponse invite, Response response) {
                                if(invite.isAccepted()){
                                    Toast.makeText(MentorPreInviteActivity.this,"Invite Request Accepted",Toast.LENGTH_SHORT).show();
                                    goToVideoActivity(1,"student");
                                }else{
                                    Log.e("checkSwitch","Response from  "+response.toString());
                                    Toast.makeText(MentorPreInviteActivity.this,"Invitation Expired",Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void failure(RetrofitError error) {
                                if(error.toString().contains("expired")){
                                    goToVideoActivity(3,"student");
                                }
                                Log.e("checkSwitch","Some error occured :"+error.toString());
                            }
                        });
                    }
                });
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                dialog.dismiss();
            }
        });
        alertDialog.show();
    }

    public void checkInviteAcceptedGoToVideoChat(){

        RestClient.get(MentorPreInviteActivity.this).getInviteData("1", mPathId, new Callback<InvitePageResponse>() {
            int peopleCount=0;
            @Override
            public void success(InvitePageResponse invitePageResponse, Response response) {

                Log.i("flaggy", "SuccessMessage" + invitePageResponse.toString());
                // success!

                extraInvitelist.addAll(invitePageResponse.getData());

                if(!extraInvitelist.isEmpty()){
                    for (InviteData inviteData:extraInvitelist) {
                        if(inviteData.isAccepted()){
                            peopleCount++;
                        }
                    }
                    goToVideoActivity(peopleCount,"mentor");
                }

            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("flaggy", "SuccessMessage" + error.toString());
                GiveErrorSortIt.SortError(error, MentorPreInviteActivity.this);
            }
        });
    }

    public void redirectToVideoChatPage(){
        final String authCode = appPrefs.getAuthCode();

        RestClient.get(MentorPreInviteActivity.this).getVidyoToken(authCode, new Callback<VidyoTokenResponseData>() {
            @Override
            public void success(VidyoTokenResponseData vidyoTokenResponseData, Response response) {
                final String vidyo_token = vidyoTokenResponseData.getVidyo_token();
                final boolean isThisMentor = DashBoardActivity.listDash.get(ApplicationUtility.getListPositionFromPathID(mPathId, "redirectDashBoard")).getIsOwner();
                final String tId = DashBoardActivity.listDash.get(ApplicationUtility.getListPositionFromPathID(mPathId, "redirectDashBoard")).getProducer().getId();
                final String name = appPrefs.getUser_FullName();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent videoIntent = new Intent(MentorPreInviteActivity.this, VideoChatActivity.class);
                        videoIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        videoIntent.putExtra("isThisMentor",isThisMentor);
                        videoIntent.putExtra("vidyo_token",vidyo_token);
                        videoIntent.putExtra("name",name);
                        videoIntent.putExtra("resourceId",mPathId);
                        videoIntent.putExtra("tId",tId);
                        startActivity(videoIntent);
                    }
                }, 350);
            }

            @Override
            public void failure(RetrofitError error) {
                GiveErrorSortIt.SortError(error, MentorPreInviteActivity.this);
            }
        });

    }

    public void goToInvitePage(){
        Intent intent = new Intent(MentorPreInviteActivity.this, InviteActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("pathID", mPathId);
        startActivity(intent);
    }

}
