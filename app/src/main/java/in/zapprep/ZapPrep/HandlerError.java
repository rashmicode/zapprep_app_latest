package in.zapprep.ZapPrep;

import java.lang.String;import java.util.List;

/**
 * Created by Tushar on 6/13/2015.
 */
public class HandlerError {
    public List<String> errors;

    public List<String> getError() {
        return errors;
    }

    public void setError(List<String> error) {
        this.errors = error;
    }
}
