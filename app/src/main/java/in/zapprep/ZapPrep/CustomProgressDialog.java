package in.zapprep.ZapPrep;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Window;
import android.widget.TextView;

/**
 * Created by Akshay on 7/1/2015.
 */
public class CustomProgressDialog extends ProgressDialog {

    TextView loadingText;
    String textDialog="Loading...";

    public CustomProgressDialog(Context context) {
        super(context);
    }

    public CustomProgressDialog(Context context, String text) {
        super(context);
        textDialog = text;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.custom_progress_dialog);
        loadingText = (TextView) findViewById(R.id.loadingText);
        loadingText.setText(textDialog);
    }

    public static ProgressDialog getCustomProgressDialog(Context context) {
        CustomProgressDialog dialog = new CustomProgressDialog(context);
        dialog.setIndeterminate(true);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        return dialog;
    }

    public static ProgressDialog getCustomProgressDialog(Context context,String text) {
        CustomProgressDialog dialog = new CustomProgressDialog(context,text);
        dialog.setIndeterminate(true);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        return dialog;
    }
}
