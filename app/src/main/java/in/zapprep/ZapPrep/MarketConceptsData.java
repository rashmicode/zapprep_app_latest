package in.zapprep.ZapPrep;

/**
 * Created by Tushar on 5/15/2015.
 */
public class MarketConceptsData {

    public String id;
    public String videoid;
    public String type;
    public String url;
    public String title;
    public String desc;

    public String getVideoID() {
        return videoid;
    }

    public void setVideoID(String videoid) {
        this.videoid = videoid;
    }

    public String image;

    public String urlThumb;

    public String getUrlThumb() {
        return urlThumb;
    }

    public void setUrlThumb(String urlThumb) {
        this.urlThumb = urlThumb;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
