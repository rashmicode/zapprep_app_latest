package in.zapprep.ZapPrep;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class FeedBackActivity extends NavigationActivity {

    final String TAG_MESSAGE = "text";
    DrawerLayout mLeftDrawerLayout;
    ListView listView;
    EditText message;
    ImageButton send;
    Firebase feedBackFireBaseRef;
    FeedBackListAdapter feedBackListAdapter;
    HashMap<String, Object> feedbackValuesMap;
    long[] timestamp;
    ArrayList<HashMap<String, Object>> feedbackMessageList = new ArrayList<HashMap<String, Object>>();
    AppPrefs appPrefs;
    String[] from = {TAG_MESSAGE};
    int[] to = {R.id.feedback_message};
    Toolbar appBar;
    TextView flagEmpty;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lay_feedback);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        appBar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(appBar);
        mLeftDrawerLayout = (DrawerLayout) mfullLayout.findViewById(R.id.drawer_layout);
        appBar.setNavigationIcon(R.drawable.pyoopil_logo_white);
        appBar.setNavigationContentDescription("BACK");

        appBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLeftDrawerLayout.openDrawer(GravityCompat.START);
            }
        });

        mLeftDrawerLayout.setDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {

            }

            @Override
            public void onDrawerOpened(View drawerView) {
                hideSoftKeyboard();
            }

            @Override
            public void onDrawerClosed(View drawerView) {

            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });

        listView = (ListView) findViewById(R.id.lv_feedback);
        message = (EditText) findViewById(R.id.fb_message_text);
        send = (ImageButton) findViewById(R.id.fb_send_button);
        flagEmpty = (TextView) findViewById(R.id.flagEmpty);
        appPrefs = new AppPrefs(this);
        Firebase.setAndroidContext(getApplicationContext());
        feedBackListAdapter = new FeedBackListAdapter(FeedBackActivity.this, feedbackMessageList);
        listView.setAdapter(feedBackListAdapter);
        feedBackFireBaseRef = new Firebase(ApplicationSingleton.FIREBASE_URL + "FeedbackUpdated/" + appPrefs.getUser_Name());

        feedBackFireBaseRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                Log.e("OnChildAdded", "" + dataSnapshot.toString());
                try {
                feedbackValuesMap = (HashMap<String, Object>) dataSnapshot.getValue();
                Date feedbackNotidate = new Date();
                feedbackNotidate.setTime((Long) feedbackValuesMap.get("timestamp"));

                Date userRegisterDate = new Date();
                userRegisterDate.setTime((Long.parseLong(new AppPrefs(FeedBackActivity.this).getCreated())));

                int diffInDays = (int) ((userRegisterDate.getTime() - feedbackNotidate.getTime()));
                    FeedBackActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            feedbackMessageList.add(feedbackValuesMap);
                            feedBackListAdapter.notifyDataSetChanged();
                            listView.setSelection(listView.getCount() - 1);
                           /* if(!showKeyboard)
                            {
                                hideSoftKeyboard();
                            }*/
                        }
                    });
                } catch (Exception e) {
                    Log.e("", "The error in thread is" + e.getLocalizedMessage() + e.getStackTrace() + e.getMessage() + e.getCause());
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });


        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String key = UUID.randomUUID().toString();
                if (message.getText() != null && !message.getText().toString().equals("") && !message.getText().toString().equals("")) {
                    new Firebase(ApplicationSingleton.FIREBASE_URL + "FeedbackUpdated/" + appPrefs.getUser_Name());
                    Firebase changeValues = new Firebase(ApplicationSingleton.FIREBASE_URL + "FeedbackUpdated/" + appPrefs.getUser_Name());
                    Map<String, Object> feedbackMap = new HashMap<String, Object>();
                    feedbackMap.put("text", message.getText().toString());
                    feedbackMap.put("timestamp", generateUnixTimestamp());
                    feedbackMap.put("displayName", appPrefs.getUser_FullName());
                    changeValues.push().setValue(feedbackMap);
                    message.setText("");
                } else {
                    Toast.makeText(FeedBackActivity.this, "Please enter some text.", Toast.LENGTH_SHORT).show();
                }
            }
        });


    }

    Long generateUnixTimestamp() {

        Long time = System.currentTimeMillis();
        return time;
    }

    public void hideSoftKeyboard() {
        if (this.getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
        }
    }

}
