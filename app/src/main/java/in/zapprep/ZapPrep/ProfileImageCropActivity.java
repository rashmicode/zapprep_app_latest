package in.zapprep.ZapPrep;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.io.File;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;
import retrofit.mime.TypedString;


public class ProfileImageCropActivity extends AppCompatActivity {

    public static AppCompatActivity profileCropActivity;
    String userImage;
    // Uri image;
    Toolbar appBar;
    String userDesc = null;
    String userTagLine = null;
    Uri croppedImageUri = null;
    String croppedImagePath = null;
    ImageView userProfile;
    EditText tagLine;
    EditText userDescription;
    String flagTag = "false";
    String flagDesc = "false";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_image_crop);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        appBar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(appBar);
        appBar.setNavigationIcon(R.drawable.pyoopil_logo_white);
        ImageView editUserPhoto = (ImageView) findViewById(R.id.userProfileImage);
        userProfile = (ImageView) findViewById(R.id.userProfileImage);
        userDescription = (EditText) findViewById(R.id.editable_user_desc);
        tagLine = (EditText) findViewById(R.id.editable_user_tag_line);
        Log.e("", "The saved instance state" + savedInstanceState);
        profileCropActivity = this;
/*

        if (savedInstanceState != null) {
            flagDesc = savedInstanceState.getString("flagDesc");
            flagTag = savedInstanceState.getString("flagTag");
        }
*/

        if (getIntent().getExtras()!=null && getIntent().getExtras().getString("userTagLine")!=null &&getIntent().getExtras().getString("userTagLine").equals(" ")) {
            tagLine.setHint("Please enter a tagline here.");
        } else {
            userTagLine = getIntent().getExtras().getString("userTagLine");
            tagLine.setText(userTagLine);
        }
        if (getIntent().getExtras()!=null && getIntent().getExtras().getString("userDesc")!=null&&getIntent().getExtras().getString("userDesc").equals(" ")) {
            userDescription.setHint("Please enter something about yourself here.");
        } else {
            userDesc = getIntent().getExtras().getString("userDesc");
            userDescription.setText(userDesc);
        }

        userDescription.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {

                Log.e("", "The text changed is" + s + "Actual" + userDescription.getText().toString());
                // you can call or do what you want with your EditText here
                userDesc = userDescription.getText().toString();
                flagDesc = "descChanged";

            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });

        tagLine.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {

                Log.e("", "The text changed is" + s + "Actual" + tagLine.getText().toString());
                // you can call or do what you want with your EditText here
                userTagLine = tagLine.getText().toString();
                flagTag = "tagChanged";


            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });

        //Checking where the control is coming from. Is it the main profile or after getting cropped?
        checkBeforeLoad();

        /*doneIndicator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callPatchForUploadingCroppedImage(croppedImagePath, userDesc, userTagLine);
            }
        });*/
        editUserPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProfileImageCropActivity.this, EditProfilePhotoActivity.class);
                intent.putExtra("userDescription", userDescription.getText().toString());
                intent.putExtra("userTagline", tagLine.getText().toString());
                intent.putExtra("profilePhoto", getIntent().getExtras().getString("profilePhoto"));
                intent.putExtra("tagChanged", flagTag);
                intent.putExtra("descChanged", flagDesc);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_profile_image_crop, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.done_sign) {
            callPatchForUploadingCroppedImage(croppedImagePath, userDesc, userTagLine);

        }
        return super.onOptionsItemSelected(item);
    }

    public void checkBeforeLoad() {
        if (getIntent().getExtras() != null) {
            if (getIntent().getExtras().getString("profilePhoto") != null) {

                userImage = getIntent().getExtras().getString("profilePhoto");
                Glide.with(this)
                        .load(userImage)
                        .centerCrop()
                        .placeholder(R.drawable.placeholder)
                        .error(R.drawable.placeholder)
                        .crossFade()
                        .dontAnimate()
                        .into(userProfile);
              /*  Ion.with(userProfile)
                        .placeholder(R.drawable.progree_animation)
                        .error(R.drawable.exclamation_error)
                        .load(userImage);*/
            } else {
                croppedImageUri = Uri.parse(getIntent().getExtras().getString("imageUri"));
                croppedImagePath = getIntent().getExtras().getString("imageEditedPath");
                userProfile.setImageURI(croppedImageUri);
            }
        }

      /*  doneIndicator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callPatchForUploadingCroppedImage(croppedImagePath, userDesc, userTagLine);
//                ProfileImageCropActivity.profileCropActivity.finish();
                //ProfileActivity.profileActivity.finish();
            }
        });*/
    }

    public void callPatchForUploadingCroppedImage(String imagePath, String userDescription, String userTagLine) {

        TypedFile coverPhotoTyped = null;
        TypedString desc = null;
        TypedString tag = null;
        final AppPrefs appPrefs = new AppPrefs(this);

        if (imagePath == null && (userDescription == null || userDescription == "") && (userTagLine == null || userTagLine == "")) {
            return;
        }
        if (imagePath != null) {
            File coverPhoto = new File(imagePath);
            Log.v("CreateStep2", "Mimetype" + coverPhoto.getAbsolutePath() + "-----" + coverPhoto.getPath() + coverPhoto.getParentFile());
            coverPhotoTyped = new TypedFile("image/jpeg", coverPhoto);
        }
        if (userDescription != null) {
            desc = new TypedString(userDescription);
        }
        if (userTagLine != null) {

            tag = new TypedString(userTagLine);
        }

        final ProgressDialog dialog = CustomProgressDialog.getCustomProgressDialog(ProfileImageCropActivity.this);
        dialog.show();

        RestClient.get(ProfileImageCropActivity.this).postPatchProfilePhoto(appPrefs.getAuthCode(), coverPhotoTyped, desc, tag, new Callback<ProfilePatchResponse>() {

            @Override
            public void success(ProfilePatchResponse profilePatchResponse, Response response) {
                Log.e("", "SuccessMessage on crop Image" + profilePatchResponse.toString());
                ProfilePatchData profilePatchData = profilePatchResponse.getData();
                String image = profilePatchData.getAvatar();
                //AppPrefs appPrefs1=new AppPrefs(ProfileImageCropActivity.this);
                Log.e("", "While setting avatar is" + image);
                if (image != null) {
                    appPrefs.setUser_Avatar(image);
                }
                dialog.hide();
                dialog.dismiss();
                Intent intent = new Intent(getApplicationContext(), ProfilePageActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);

            }


            @Override
            public void failure(RetrofitError error) {

                dialog.hide();
                dialog.dismiss();

                String errorShown = error.getLocalizedMessage().toString();
                //Jugaad
                if (errorShown.equals("No key was sent in the body")) {
                    Intent intent = new Intent(getApplicationContext(), ProfilePageActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                } else {
                    Toast.makeText(ProfileImageCropActivity.this, errorShown, Toast.LENGTH_SHORT).show();
                }
                GiveErrorSortIt.SortError(error, ProfileImageCropActivity.this);

                // something went wrong
            }
        });

    }

}
