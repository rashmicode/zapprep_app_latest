package in.zapprep.ZapPrep;

import java.util.List;

/**
 * Created by PoolDead on 7/3/2018.
 */

public class ResumeStatusData {
    String quizStatus;
    List<QuestionData> questions;
    String duration;
    String practiceCount;
    String startIndex;

    public String getQuizStatus() {
        return quizStatus;
    }

    public void setQuizStatus(String quizStatus) {
        this.quizStatus = quizStatus;
    }

    public List<QuestionData> getQuestions() {
        return questions;
    }

    public void setQuestions(List<QuestionData> questions) {
        this.questions = questions;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getPracticeCount() {
        return practiceCount;
    }

    public void setPracticeCount(String practiceCount) {
        this.practiceCount = practiceCount;
    }

    public String getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(String startIndex) {
        this.startIndex = startIndex;
    }

    @Override
    public String toString() {
        return "ResumeStatusData{" +
                "quizStatus='" + quizStatus + '\'' +
                ", questions=" + questions +
                ", duration='" + duration + '\'' +
                ", practiceCount='" + practiceCount + '\'' +
                ", startIndex='" + startIndex + '\'' +
                '}';
    }

}
