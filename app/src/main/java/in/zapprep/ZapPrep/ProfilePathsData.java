package in.zapprep.ZapPrep;

import java.util.List;

/**
 * Created by Tushar on 6/11/2015.
 */
public class ProfilePathsData {

    public String title;
    public String desc;
    public String coverPhoto;
    public String coverPhotoThumb;
    public MarketProducerData producer;
    public String studentCount;

    public List<ConceptData> concepts;
    public String qbId;
    public String mentorRoomId;
    public String isOwnwer;
    public String id;
    public String allowEnroll;
    public String rating;

    public String mentorPaid;
    public String coursePaid;
    public String mentorAvailable;
    public String courseINR;
    public String courseUSD;

    public String getMentorPaid() {
        return mentorPaid;
    }

    public void setMentorPaid(String mentorPaid) {
        this.mentorPaid = mentorPaid;
    }

    public String getCoursePaid() {
        return coursePaid;
    }

    public void setCoursePaid(String coursePaid) {
        this.coursePaid = coursePaid;
    }

    public String getMentorAvailable() {
        return mentorAvailable;
    }

    public void setMentorAvailable(String mentorAvailable) {
        this.mentorAvailable = mentorAvailable;
    }

    public String getCourseINR() {
        return courseINR;
    }

    public void setCourseINR(String courseINR) {
        this.courseINR = courseINR;
    }

    public String getCourseUSD() {
        return courseUSD;
    }

    public void setCourseUSD(String courseUSD) {
        this.courseUSD = courseUSD;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }



    public MarketProducerData getProducer() {
        return producer;
    }

    public void setProducer(MarketProducerData producer) {
        this.producer = producer;
    }

    public List<ConceptData> getConcepts() {
        return concepts;
    }

    public void setConcepts(List<ConceptData> concepts) {
        this.concepts = concepts;
    }

    public String getAllowEnroll() {
        return allowEnroll;
    }

    public void setAllowEnroll(String allowEnroll) {
        this.allowEnroll = allowEnroll;
    }

    public ProfilePathsData() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getCoverPhoto() {
        return coverPhoto;
    }

    public void setCoverPhoto(String coverPhoto) {
        this.coverPhoto = coverPhoto;
    }

    public String getCoverPhotoThumb() {
        return coverPhotoThumb;
    }

    public void setCoverPhotoThumb(String coverPhotoThumb) {
        this.coverPhotoThumb = coverPhotoThumb;
    }

    public String getStudentCount() {
        return studentCount;
    }

    public void setStudentCount(String studentCount) {
        this.studentCount = studentCount;
    }

    public String getQbId() {
        return qbId;
    }

    public void setQbId(String qbId) {
        this.qbId = qbId;
    }

    public String getMentorRoomId() {
        return mentorRoomId;
    }

    public void setMentorRoomId(String mentorRoomId) {
        this.mentorRoomId = mentorRoomId;
    }

    public String getIsOwnwer() {
        return isOwnwer;
    }

    public void setIsOwnwer(String isOwnwer) {
        this.isOwnwer = isOwnwer;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
