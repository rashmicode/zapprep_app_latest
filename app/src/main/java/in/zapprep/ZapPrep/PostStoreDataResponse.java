package in.zapprep.ZapPrep;

/**
 * Created by PoolDead on 7/4/2018.
 */

public class PostStoreDataResponse {
    PostStoreData data;

    public PostStoreData getData() {
        return data;
    }

    public void setData(PostStoreData data) {
        this.data = data;
    }
}
