package in.zapprep.ZapPrep;

/**
 * Created by PoolDead on 11/25/2017.
 */

public class StatusResponse {
    private boolean onlineStatus;

    public boolean isOnlineStatus() {
        return onlineStatus;
    }

    public void setOnlineStatus(boolean onlineStatus) {
        this.onlineStatus = onlineStatus;
    }
}
