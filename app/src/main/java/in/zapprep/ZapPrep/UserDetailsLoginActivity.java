package in.zapprep.ZapPrep;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedString;


public class UserDetailsLoginActivity extends ActionBarActivity {
    Toolbar appBar;
    ProgressDialog dialog;
    TextView message;
    TextView messageEmail;
    EditText userName;
    EditText userEmail;
    Button done;
    Boolean changeEmail=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_name);
        appBar = (Toolbar) findViewById(R.id.app_bar);

        setSupportActionBar(appBar);
        appBar.setNavigationIcon(R.drawable.pyoopil_logo_white);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        if (getIntent().getExtras() != null) {
            changeEmail = getIntent().getBooleanExtra("changeEmail", false);
        }

        message = (TextView) findViewById(R.id.message);
        messageEmail = (TextView) findViewById(R.id.messageEmail);
        userName = (EditText) findViewById(R.id.usernameNew);
        userEmail = (EditText) findViewById(R.id.useremailNew);
        done = (Button) findViewById(R.id.username_button);

        message.setTypeface(ApplicationSingleton.robotoRegular);
        messageEmail.setTypeface(ApplicationSingleton.robotoRegular);
        userEmail.setTypeface(ApplicationSingleton.robotoRegular);
        userName.setTypeface(ApplicationSingleton.robotoRegular);
        message.setTextColor(getResources().getColor(R.color.secondaryText));
        messageEmail.setTextColor(getResources().getColor(R.color.secondaryText));
        String userNameAssigned = new AppPrefs(this).getUser_Name();
        if (changeEmail) {
            messageEmail.setVisibility(View.VISIBLE);
            userEmail.setVisibility(View.VISIBLE);
            message.setText("Please enter a username. This will be your unique identity on ZapPrep.");
            messageEmail.setText("Please enter your email address.");
        }
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (changeEmail && (userEmail.getText() == null || userEmail.getText().toString().equals("")) || userEmail.getText().toString().equals(" ")) {
                    Toast.makeText(UserDetailsLoginActivity.this, "Please enter an Email Id", Toast.LENGTH_SHORT).show();
                } else if (userName.getText() == null || userName.getText().toString().equals("") || userName.getText().toString().equals(" ")) {
                    Toast.makeText(UserDetailsLoginActivity.this, "Please enter a username", Toast.LENGTH_SHORT).show();
                } else {
                    sendPatchToUpdateUserDetail();
                }
            }


        });
        dialog = CustomProgressDialog.getCustomProgressDialog(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_user_name, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void sendPatchToUpdateUserDetail() {
        Log.e("", "Entered method to change userName");
        final AppPrefs appPrefs = new AppPrefs(UserDetailsLoginActivity.this);
        TypedString user = null;
        TypedString email= null;
        String usernameNew = userName.getText().toString();
        String userEmailNew;
        if(changeEmail)
        {
            userEmailNew = userEmail.getText().toString();
            email= new TypedString(userEmailNew);
        }
        user = new TypedString(usernameNew);
        dialog.show();
        RestClient.get(this).patchUserName(appPrefs.getAuthCode(), user, email, new Callback<ProfilePatchResponse>() {
            @Override
            public void success(ProfilePatchResponse profilePatchResponse, Response response) {

                Toast.makeText(UserDetailsLoginActivity.this, "UserName Updated", Toast.LENGTH_SHORT).show();
                appPrefs.setUser_Name(userName.getText().toString());
                dialog.hide();
                dialog.dismiss();
                Intent displayAct = new Intent(UserDetailsLoginActivity.this, DashBoardActivity.class);
                displayAct.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(displayAct);
            }

            @Override
            public void failure(RetrofitError error) {
                dialog.hide();
                dialog.dismiss();
                Log.e("", "The error here in username change is" + error.getLocalizedMessage());
                Toast.makeText(UserDetailsLoginActivity.this, error.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                GiveErrorSortIt.SortError(error, UserDetailsLoginActivity.this);
            }
        });
    }

    @Override
    public void onBackPressed() {
        Toast.makeText(UserDetailsLoginActivity.this, "Please enter the username to proceed", Toast.LENGTH_SHORT).show();
    }

}
