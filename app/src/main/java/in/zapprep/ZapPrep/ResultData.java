package in.zapprep.ZapPrep;

import java.util.List;

/**
 * Created by PoolDead on 7/6/2018.
 */

public class ResultData {
    String name;
    List<String> choices;
    String mark;
    List<String> yourAnswer;
    String status;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getChoices() {
        return choices;
    }

    public void setChoices(List<String> choices) {
        this.choices = choices;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public List<String> getYourAnswer() {
        return yourAnswer;
    }

    public void setYourAnswer(List<String> yourAnswer) {
        this.yourAnswer = yourAnswer;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "ResultData{" +
                "name='" + name + '\'' +
                ", choices=" + choices +
                ", mark='" + mark + '\'' +
                ", yourAnswer=" + yourAnswer +
                ", status='" + status + '\'' +
                '}';
    }
}
