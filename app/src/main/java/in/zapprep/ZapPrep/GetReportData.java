package in.zapprep.ZapPrep;

import java.util.List;

/**
 * Created by PoolDead on 7/6/2018.
 */

public class GetReportData {
    String _id;
    String practiceCount;
    String timeTaken;
    String percentage;
    String correct;
    String reduced;
    String inCorrectCount;
    String totalMarks;
    String negativeMark;
    String finalMark;
    String createdAt;
    List<ResultData> result;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getPracticeCount() {
        return practiceCount;
    }

    public void setPracticeCount(String practiceCount) {
        this.practiceCount = practiceCount;
    }

    public String getTimeTaken() {
        return timeTaken;
    }

    public void setTimeTaken(String timeTaken) {
        this.timeTaken = timeTaken;
    }

    public String getPercentage() {
        return percentage;
    }

    public void setPercentage(String percentage) {
        this.percentage = percentage;
    }

    public String getCorrect() {
        return correct;
    }

    public void setCorrect(String correct) {
        this.correct = correct;
    }

    public String getReduced() {
        return reduced;
    }

    public void setReduced(String reduced) {
        this.reduced = reduced;
    }

    public String getInCorrectCount() {
        return inCorrectCount;
    }

    public void setInCorrectCount(String inCorrectCount) {
        this.inCorrectCount = inCorrectCount;
    }

    public String getTotalMarks() {
        return totalMarks;
    }

    public void setTotalMarks(String totalMarks) {
        this.totalMarks = totalMarks;
    }

    public String getNegativeMark() {
        return negativeMark;
    }

    public void setNegativeMark(String negativeMark) {
        this.negativeMark = negativeMark;
    }

    public String getFinalMark() {
        return finalMark;
    }

    public void setFinalMark(String finalMark) {
        this.finalMark = finalMark;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public List<ResultData> getResult() {
        return result;
    }

    public void setResult(List<ResultData> result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "GetReportData{" +
                "_id='" + _id + '\'' +
                ", practiceCount='" + practiceCount + '\'' +
                ", timeTaken='" + timeTaken + '\'' +
                ", percentage='" + percentage + '\'' +
                ", correct='" + correct + '\'' +
                ", reduced='" + reduced + '\'' +
                ", inCorrectCount='" + inCorrectCount + '\'' +
                ", totalMarks='" + totalMarks + '\'' +
                ", negativeMark='" + negativeMark + '\'' +
                ", finalMark='" + finalMark + '\'' +
                ", createdAt='" + createdAt + '\'' +
                ", result=" + result +
                '}';
    }
}
