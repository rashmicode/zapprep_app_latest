package in.zapprep.ZapPrep;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;


public class PeoplePageActivity extends NavigationActivity {

    Toolbar appBar;
    DrawerLayout mLeftDrawerLayout;
    String mPathId;
    TextView mNomatch;
    int objectPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_people_page);
        appBar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(appBar);
        mLeftDrawerLayout = (DrawerLayout) mfullLayout.findViewById(R.id.drawer_layout);
        mNomatch = (TextView) findViewById(R.id.noMatch);
        appBar.setNavigationIcon(R.drawable.pyoopil_logo_white);
        appBar.setNavigationContentDescription("BACK");

        appBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLeftDrawerLayout.openDrawer(GravityCompat.START);
                ApplicationSingleton.getInstance().hideSoftKeyboard(PeoplePageActivity.this);
            }
        });
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        if(getIntent().getExtras()!=null)
        {
            mPathId= getIntent().getExtras().getString("pathID");
            if(getIntent().getStringExtra("flag")!=null)
            {
                objectPosition=getIntent().getIntExtra("position",0);
            }
        }
       // String pathId = DashBoardAdapter.listDash.get(gridPosition).getId();


        if (savedInstanceState == null) {
            PeoplePageActivityFragment peoplePageActivityFragment = PeoplePageActivityFragment.newInstance(mPathId, "activityRedirect");
            getSupportFragmentManager().beginTransaction().add(R.id.fragment, peoplePageActivityFragment).commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.



        return false;
    }

    @Override
    public void onBackPressed()
    {
        if(getIntent().getStringExtra("flag")!=null) {
            Intent i = new Intent(PeoplePageActivity.this, ConsumeCourseConceptActivity.class);
            //marketData.getParcelableArrayListExtra(listObject);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.putExtra("position", objectPosition);
            i.putExtra("pathID", mPathId);
            startActivity(i);
        }
        else{
            Intent intent = new Intent(PeoplePageActivity.this, ConsumeCoursePage.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("pathID", mPathId);
            startActivity(intent);
        }
    }
}
