package in.zapprep.ZapPrep;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.crashlytics.android.Crashlytics;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
//import in.zapprep.ZapPrep.ChatFragment.isSessionActiveListener;
import in.zapprep.ZapPrep.Notifications.NotificationActivity;
//import com.quickblox.chat.model.QBDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class ConsumeCoursePage extends NavigationActivity implements ConsumeCoursePageFragment.RedirectActivityReplaceFragment {
    //public class ConsumeCoursePage extends NavigationActivity implements isSessionActiveListener, ConsumeCoursePageFragment.RedirectActivityReplaceFragment {
    DrawerLayout mLeftDrawerLayout;

    Boolean open = false;
    static String mPathId;
    private Toolbar mAppBar;
    private AppPrefs appPrefs;
    public static List<PeopleData> list;
    public static List<String> userNameList = new ArrayList<String>();
    public static List<String> fullNameList = new ArrayList<String>();
    public static Map<String, PeopleData> userMap = new HashMap<String, PeopleData>();
    public static Map<String, String> userNametoIdMap = new HashMap<String, String>();
    ProgressDialog dialog;
    Menu mMenu;
    int page = 1;
    //int count = 0;
    String flag;
    public static String title = "CoursePage";
    Spinner toolBarDropDown;
    int pos;
    Button courseShareButton;
    JSONObject propsChannel;
    JSONObject propsHamburger;
    Spinner spinner;
    public static DashBoardData mPathData;
    public static boolean showMentorDialogResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consume_course_page);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        //mLeftDrawerLayout = (DrawerLayout) mfullLayout.findViewById(R.id.drawer_layout);
        mLeftDrawerLayout=super.mDrawerLayout;
        appPrefs = new AppPrefs(this);
        mAppBar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(mAppBar);

        mAppBar.setNavigationIcon(R.drawable.pyoopil_logo_white);
        mAppBar.setNavigationContentDescription("BACK");

        courseShareButton = (Button) findViewById(R.id.course_shareButton);

        if(courseShareButton.getVisibility()==View.INVISIBLE)
        {
            courseShareButton.setVisibility(View.GONE);
        }
        //startService(new Intent(ConsumeCoursePage.this, UnreadAggregationService.class));

        if (getIntent().getExtras() != null) {
            mPathId = getIntent().getExtras().getString("pathID");
            if (getIntent().getExtras().getString("exploreCourseRedirect") != null) {
                flag = "ExploreCourseRedirect";
            }
        }

        for (DashBoardData dashBoardData : DashBoardActivity.listDash) {
            if (dashBoardData.getId().equals(mPathId)) {
                title = dashBoardData.getTitle();
            }
            if (dashBoardData.getId().equals(mPathId)) {
                mPathData = dashBoardData;
            }
        }
        propsChannel = new JSONObject();
        propsHamburger = new JSONObject();

        final List<String> listItems = new ArrayList<>();
        listItems.add("Review");

        View spinnerContainer = LayoutInflater.from(this).inflate(R.layout.toolbar_spinner,
                mAppBar, false);
        ActionBar.LayoutParams lp = new ActionBar.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        mAppBar.addView(spinnerContainer, lp);

        spinner = (Spinner) spinnerContainer.findViewById(R.id.toolbar_spinner);
        spinner.setPrompt(title);
        //spinner.setOnItemSelectedListener(new CustomOnItemSelectedListener());
        SpinnerAdapter spinnerAdapter = new SpinnerAdapter(this, title, mPathId, spinner, "coursePage");
        spinnerAdapter.addItems(listItems);
        spinner.setAdapter(spinnerAdapter);
//        spinner.setOnItemSelectedListener(new CustomOnItemSelectedListener());

        ConsumeCoursePage.this.setTitle(title);

        mAppBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLeftDrawerLayout.openDrawer(GravityCompat.START);
            }
        });

        mLeftDrawerLayout.setDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {

            }

            @Override
            public void onDrawerOpened(View drawerView) {

                hideSoftKeyboard();
            }

            @Override
            public void onDrawerClosed(View drawerView) {

            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });

        ConsumeCoursePageFragment consumeCoursePageFragment = ConsumeCoursePageFragment.newInstance(mPathId);
        getSupportFragmentManager().beginTransaction().addToBackStack(null).add(R.id.center_container, consumeCoursePageFragment, "FRAGMENT").commit();
        dialog = CustomProgressDialog.getCustomProgressDialog(this);
        userNametoIdMap = new HashMap<String, String>();
        fullNameList = new ArrayList<String>();
        userMap = new HashMap<String, PeopleData>();

        getPeopleDataInCourse(mPathId, page);
        //Redirect to Consume Course and People Page
        courseShareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Event -ShareCourse
                JSONObject propsShare = new JSONObject();
                try {
                    propsShare.put("Title", DashBoardActivity.listDash.get(ApplicationUtility.getListPositionFromPathID(mPathId, "redirectDashBoard")).getTitle());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                mixpanel.track("ShareCourse", propsShare);
                String url;
                if (DashBoardActivity.listDash.get(ApplicationUtility.getListPositionFromPathID(mPathId, "redirectDashBoard")).getProducer() != null) {
                    url = "http://www.pyoopil.com/profile/" + DashBoardActivity.listDash.get(ApplicationUtility.getListPositionFromPathID(mPathId, "redirectDashBoard")).getProducer().getId().trim() + "/" + DashBoardActivity.listDash.get(ApplicationUtility.getListPositionFromPathID(mPathId, "redirectDashBoard")).getProducer().getUsername().trim() + "/course/" + mPathId + "/" + DashBoardActivity.listDash.get(ApplicationUtility.getListPositionFromPathID(mPathId, "redirectDashBoard")).getTitle().trim();
                } else {
                    url = "http://www.pyoopil.com/profile/" + appPrefs.getUser_Id() + "/" + appPrefs.getUser_Name() + "/course/" + mPathId + "/" + DashBoardActivity.listDash.get(ApplicationUtility.getListPositionFromPathID(mPathId, "redirectDashBoard")).getTitle().trim();
                }
                String appUrl = "https://goo.gl/AjwSVn";
                String replacedUrl = url.replaceAll(" ", "-");
                Log.e("Explore Course", "Share button clicked");
                Intent i = new Intent(android.content.Intent.ACTION_SEND);
                i.setType("text/plain");
                i.putExtra(android.content.Intent.EXTRA_TEXT, "Hey! I am taking a course on Pyoopil - " + DashBoardActivity.listDash.get(ApplicationUtility.getListPositionFromPathID(mPathId, "redirectDashBoard")).getTitle() + ". Download the app from this link -\n" + appUrl + "\n\nand enroll into this course -  \n" + replacedUrl);
                       /* marketData.putExtra(android.content.Intent.EXTRA_SUBJECT, "The course details are : ");
                        marketData.putExtra(android.content.Intent.EXTRA_TEXT, replacedUrl + "\n");*/
                startActivity(Intent.createChooser(i, "Share via"));
            }
        });
    }

    @Override
    public void onBackPressed() {
//        ChatFragment test = (ChatFragment) getSupportFragmentManager().findFragmentByTag("testID");
//        AnnouncementsFragment test1 = (AnnouncementsFragment) getSupportFragmentManager().findFragmentByTag("testID1");
//        PeoplePageActivityFragment test2 = (PeoplePageActivityFragment) getSupportFragmentManager().findFragmentByTag("testID2");
//        CourseRatingFragment courseRatingFragment = (CourseRatingFragment) getSupportFragmentManager().findFragmentByTag("coursePage");
//        if (test != null && test.isVisible() || test1 != null && test1.isVisible() || test2 != null && test2.isVisible() || courseRatingFragment != null && courseRatingFragment.isVisible()) {
//            Intent consumeIntent = new Intent(ConsumeCoursePage.this, ConsumeCoursePage.class);
//            consumeIntent.putExtra("pathID", mPathId);
//            startActivity(consumeIntent);
//            //DO STUFF
//        } else {
            //Whatever
            Intent dashIntent = new Intent(ConsumeCoursePage.this, DashBoardActivity.class);
            dashIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(dashIntent);
        //}
    }

    private void unSetUpar() {
        unSetRooms(false);
    }

    private void unSetAll() {
        unSetRooms(true);
    }

    private void unSetRooms(boolean flag) {
        if (flag) {

        }
    }

    private void getPeopleDataInCourse(final String mPathID, final int page) {

//
        RestClient.get(ConsumeCoursePage.this).getPeoplePageData(String.valueOf(page), mPathID, new Callback<PeoplePageResponse>() {

            @Override
            public void success(PeoplePageResponse peoplePageResponse, Response response) {
                Log.i("", "SuccessMessage" + peoplePageResponse.toString());
                // success!
                list = peoplePageResponse.getData();
                for (PeopleData peopleData : list) {
                    userNametoIdMap.put(peopleData.getUsername(), peopleData.getId());
                    userMap.put(peopleData.getQbId(), peopleData);
                    userNameList.add(peopleData.getUsername());
                    String fullNameMention = peopleData.getName() + " (@" + peopleData.getUsername() + ")";
                    fullNameList.add(fullNameMention);
                }
                int j = page;
                if (peoplePageResponse.getMore().equals("true")) {
                    getPeopleDataInCourse(mPathID, j++);
                    dialog.show();
                } else {
                    dialog.hide();
                    dialog.dismiss();
                }
               /* }
               */
            }

            @Override
            public void failure(RetrofitError error) {
                dialog.hide();
                dialog.dismiss();
                GiveErrorSortIt.SortError(error, ConsumeCoursePage.this);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_consume_course_list, menu);

        mMenu = menu;
        final TextView count_textview;
        Firebase fireBaseCountRef;


        final View menu_notification = menu.findItem(R.id.notificationicon).getActionView();
        count_textview = (TextView) menu_notification.findViewById(R.id.tv_unread_count);
        count_textview.setVisibility(View.INVISIBLE);

        Firebase.setAndroidContext(getApplicationContext());
        fireBaseCountRef = new Firebase(ApplicationSingleton.FIREBASE_URL + "users/" + appPrefs.getUser_Id() + "/");

        fireBaseCountRef.child("count").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() == null) {
                    //  Log.e("", "Its empty bro in consume course");
                } else {
                    countNot = Integer.parseInt(dataSnapshot.getValue().toString());
                }
                ApplicationUtility.updateUnreadCount(countNot, ConsumeCoursePage.this, count_textview);
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });


        menu_notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ConsumeCoursePage.this, NotificationActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_from_bottom, R.anim.stay);

                Firebase changeValues = new Firebase(ApplicationSingleton.FIREBASE_URL + "users/" + appPrefs.getUser_Id() + "/");
                changeValues.child("count").setValue(0l);


            }
        });


        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return super.onOptionsItemSelected(item);
    }

//    @Override
//    public void disableDrawer(Boolean disable) {
//        if (disable) {
//            mLeftDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
//            mAppBar.setNavigationOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//                }
//            });
//
//        } else {
//            mLeftDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
//            mAppBar.setNavigationOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    mLeftDrawerLayout.openDrawer(GravityCompat.START);
//                }
//            });
//
//        }
//    }

    public void hideSoftKeyboard() {
        if (this.getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
        }
    }

    @Override
    public void redirectNotificationMentorChannel() {

    }

    @Override
    public void sendMentorChannelFlag(boolean mentorDialogWhenResponse) {
        showMentorDialogResponse = mentorDialogWhenResponse;
    }
}
