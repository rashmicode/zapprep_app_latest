package in.zapprep.ZapPrep;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import java.util.Collections;
import java.util.concurrent.TimeUnit;

public class QuizObjectActivity extends AppCompatActivity {

    String title="";
    String objId="";
    String authCode="";
    TextView quizTitle=null;
    TextView alreadyTakenOnetime=null;
    TextView alreadyTakenPractice=null;
    TextView playText=null;
    TextView resumeText=null;
    LinearLayout emptyText=null;
    TextView quizTimerText=null;
    TextView quesMark=null;
    TextView quesIndex=null;
    TextView viewReportTxt=null;
    TextView tryAgainTxt=null;
    LinearLayout playCardView=null;
    LinearLayout alreadyTakenCardView=null;
    LinearLayout quizCardView=null;
    QuizTimer quizTimer=null;
    AppPrefs appPrefs=null;
    QuizData quizData=null;
    int practiceCount=0;
    int maxPracticeCount=0;
    int selectedIndex=0;
    boolean quizResumeStatus=false;
    List<QuestionData> questionData;
    QuestionData selectedQuestion=null;
    ResumeStatusData resumeData=null;
    ProgressDialog dialog=null;
    FloatingActionButton playBtn=null;
    ProgressBar quizProgress=null;
    LinearLayout questionCard=null;
    Button submitAnswerBtn=null;
    Button viewReportBtn=null;
    LinearLayout quizReportOneTime=null;
    LinearLayout quizReportPractice=null;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_object);

        quizTitle=(TextView)findViewById(R.id.quizTitle);
        playCardView=(LinearLayout)findViewById(R.id.play_card);
        alreadyTakenCardView=(LinearLayout)findViewById(R.id.already_taken_card);
        quizCardView=(LinearLayout)findViewById(R.id.quiz_card);
        quizReportOneTime=(LinearLayout)findViewById(R.id.quiz_report_one_time);
        quizReportPractice=(LinearLayout)findViewById(R.id.quiz_report_practice);

        alreadyTakenOnetime = (TextView)findViewById(R.id.already_taken_onetime);
        alreadyTakenPractice = (TextView)findViewById(R.id.already_taken_practice);

        playText = (TextView)findViewById(R.id.play_text);
        resumeText = (TextView)findViewById(R.id.resume_text);

        emptyText = (LinearLayout)findViewById(R.id.empty_text);

        playBtn = (FloatingActionButton)findViewById(R.id.play_btn);
        quesIndex = (TextView)findViewById(R.id.ques_index);
        quizTimerText = (TextView)findViewById(R.id.quiz_timer);
        questionCard = (LinearLayout)findViewById(R.id.question_card);
        quesMark = (TextView)findViewById(R.id.ques_mark);
        quizProgress = (ProgressBar)findViewById(R.id.quiz_progress);
        submitAnswerBtn = (Button)findViewById(R.id.submit_answer_btn);

        viewReportBtn = (Button)findViewById(R.id.view_report_btn);
        viewReportTxt = (TextView)findViewById(R.id.view_report_txt);
        tryAgainTxt = (TextView)findViewById(R.id.try_again_txt);

        appPrefs = new AppPrefs(this);
        authCode = appPrefs.getAuthCode();

        dialog = CustomProgressDialog.getCustomProgressDialog(QuizObjectActivity.this);

        if(getIntent().getExtras()!=null){
            if(getIntent().getStringExtra("title")!=null){
                title=getIntent().getExtras().getString("title");
                quizTitle.setText(title);
            }
            if(getIntent().getStringExtra("objId")!=null){
                objId=getIntent().getExtras().getString("objId");
                initQuiz(objId);
            }
        }

        playBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (objId != null) {
                    if (!quizResumeStatus) {
                        startFreshQuiz(objId);
                        Log.e("Qz-playBtn","false");
                    } else {
                        quizResume(resumeData);
                        Log.e("Qz-playBtn","true");
                    }
                }
            }
        });

        submitAnswerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(selectedQuestion!=null){
                    boolean locHasMultipleAnswer = Boolean.parseBoolean(selectedQuestion.isHasMultipleAnswer());
                    int locMaxAnswer = Integer.parseInt(selectedQuestion.getMaxAnswer());
                    List<String> answers = new ArrayList<>();
                    int startTime = parseToSeconds(quizTimerText.getText().toString());
                    int startIndex = selectedIndex + 1;
                    String qId = selectedQuestion.getId();

                    if(!locHasMultipleAnswer){
                        for(int i = 0; i < questionCard.getChildCount(); i++) {
                            View child = questionCard.getChildAt(i);
                            if(child instanceof RadioGroup ) {
                                RadioGroup radio = (RadioGroup)child;
                                if(radio.getCheckedRadioButtonId()>0){
                                    answers.add(""+radio.getCheckedRadioButtonId());
                                   submitAnswer(answers,""+startTime,""+startIndex,qId);
                                }else{
                                    Toast.makeText(QuizObjectActivity.this,"Please select an option", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                    }else{
                        for(int i = 0; i < questionCard.getChildCount(); i++) {
                            View child = questionCard.getChildAt(i);
                            if(child instanceof CheckBox) {
                                CheckBox cb = (CheckBox)child;
                                if(cb.isChecked()){
                                    answers.add(""+cb.getId());
                                }
                            }
                        }
                        if(answers.size()<=locMaxAnswer && answers.size()>=1){
                            submitAnswer(answers,""+startTime,""+startIndex,qId);
                        }else{
                            Toast.makeText(QuizObjectActivity.this,"You can only check/tick up to "+locMaxAnswer+" checkboxes", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        });

        viewReportBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getReport();
                hideAlreadyTaken();
            }
        });

        viewReportTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getReport();
                hidePlay();
            }
        });

        tryAgainTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initQuiz(objId);
            }
        });

    }

    public void initQuiz(final String objId){
        dialog.show();
        RestClient.get(QuizObjectActivity.this).getQuizData(authCode, objId, new Callback<QuizDataResponse>() {

            @Override
            public void success(QuizDataResponse quizDataResponse, Response response) {
                //success
                quizData = quizDataResponse.getData();
                if(Boolean.parseBoolean(quizData.quizType)){
                    maxPracticeCount = Integer.parseInt(quizData.practiceCount);
                    practiceCount = 1;
                    initPracticeCount(objId,practiceCount);
                    Log.e("Qz-initQuiz","true");
                }else{
                    maxPracticeCount = 0;
                    practiceCount = 0;
                    initPracticeCount(objId,practiceCount);
                    Log.e("Qz-initQuiz","false");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (dialog != null && dialog.isShowing()) {
                    dialog.hide();
                    dialog.dismiss();
                }
            }
        });
    }

    public void initPracticeCount(final String objId,int pc){
        RestClient.get(QuizObjectActivity.this).getPracticeCount(authCode, objId,""+pc , new Callback<PracticeCountDataResponse>() {

            @Override
            public void success(PracticeCountDataResponse practiceCountDataResponse, Response response) {
                //success
                final PracticeCountData practiceCountData = practiceCountDataResponse.getData();
                int locPracticeCount = Integer.parseInt(practiceCountData.getPracticeCount());

                if(Boolean.parseBoolean(practiceCountData.getStatus())){
                    if(locPracticeCount==1){
                        showAlreadyTaken(0);
                        Log.e("Qz-initPracticeCount-ot","true");
                    }else{
                        practiceCount = locPracticeCount;
                        showPlay();
                        initResumeQuizStatus(objId,practiceCount);
                        Log.e("Qz-initPracticeCount-ot","false");
                    }
                }else{
                    if(maxPracticeCount==locPracticeCount){
                        showAlreadyTaken(1);
                        Log.e("Qz-initPracticeCount-p","true");
                    }else{
                        practiceCount = locPracticeCount+1;
                        showPlay();
                        initResumeQuizStatus(objId,practiceCount);
                        Log.e("Qz-initPracticeCount-p","false");
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (dialog != null && dialog.isShowing()) {
                    dialog.hide();
                    dialog.dismiss();
                }
            }
        });
    }

    public void initResumeQuizStatus(final String objId,final int pc){
        RestClient.get(QuizObjectActivity.this).getQuizResumeStatus(authCode, objId,""+pc , new Callback<ResumeStatusDataResponse>() {

            @Override
            public void success(ResumeStatusDataResponse resumeStatusDataResponse, Response response) {
                //success
                dialog.hide();
                dialog.dismiss();

                final ResumeStatusData resumeStatusData = resumeStatusDataResponse.getData();

                if(Boolean.parseBoolean(quizData.quizType) && pc>1){
                    viewReportTxt.setVisibility(View.VISIBLE);
                }

                if (Boolean.parseBoolean(resumeStatusData.getQuizStatus())) {
                    quizResumeStatus = false;
                    playText.setVisibility(View.VISIBLE);
                    resumeText.setVisibility(View.GONE);
                    Log.e("Qz-initResumeQuizStatus","false");
                } else {
                    quizResumeStatus = true;
                    resumeText.setVisibility(View.VISIBLE);
                    playText.setVisibility(View.GONE);
                    resumeData = resumeStatusData;
                    Log.e("Qz-initResumeQuizStatus","false");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (dialog != null && dialog.isShowing()) {
                    dialog.hide();
                    dialog.dismiss();
                }
            }
        });
    }

    public void startFreshQuiz(final String objId){
        dialog.show();
        RestClient.get(QuizObjectActivity.this).getQuestions(authCode, objId, new Callback<QuestionDataResponse>() {

            @Override
            public void success(QuestionDataResponse questionDataResponse, Response response) {
                //success
                questionData = questionDataResponse.getData();

                if (questionData.size()!=0) {
                    Collections.shuffle(questionData);
                    selectedIndex = 0;
                    selectedQuestion = questionData.get(selectedIndex);
                    hidePlay();
                    showQuiz();
                    quizProgress.setProgress(0);
                    initQuizForm();
                    storeQuizResumeDetails(questionData, objId, practiceCount);
                    long millisInFuture = Long.parseLong(quizData.getDuration()) * 60000;
                    if(quizTimer!=null){
                        quizTimer.cancel();
                        quizTimer=null;
                    }
                    quizTimer = new QuizTimer(millisInFuture, 1000);
                    quizTimer.start();
                } else {
                    hidePlay();
                    showEmpty();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (dialog != null && dialog.isShowing()) {
                    dialog.hide();
                    dialog.dismiss();
                }
            }
        });
    }

    public void quizResume(ResumeStatusData data){
        int localIndex = Integer.parseInt(data.startIndex);
        questionData = data.questions;
        practiceCount = Integer.parseInt(data.practiceCount);
        selectedIndex = localIndex == -1 ? 0 : localIndex;
        selectedQuestion = questionData.get(selectedIndex);
        hidePlay();
        showQuiz();
        quizProgress.setProgress((int)Math.ceil((selectedIndex*100/questionData.size())));
        initQuizForm();
        long millisInFuture = Long.parseLong(data.getDuration()) * 1000;
        if(quizTimer!=null){
            quizTimer.cancel();
            quizTimer=null;
        }
        quizTimer = new QuizTimer(millisInFuture, 1000);
        quizTimer.start();
    }

    public void initQuizForm(){
        boolean locHasMultipleAnswer = Boolean.parseBoolean(selectedQuestion.isHasMultipleAnswer());
        int locMaxAnswer = Integer.parseInt(selectedQuestion.getMaxAnswer());
        String[] locChoices = selectedQuestion.getChoices();
        String locMark = selectedQuestion.getMark();
        String locQuestionName = selectedQuestion.getName();
        String[] locImages = selectedQuestion.getImages();

        TextView questionView = new TextView(QuizObjectActivity.this);
        ImageView imageView = new ImageView(QuizObjectActivity.this);
        RadioGroup radioGroup = new RadioGroup(QuizObjectActivity.this);

        questionCard.removeAllViews();
        questionCard.setPadding(10,10,10,10);

        if(locQuestionName!=null){
            questionView.setPadding(5,10,5,10);
            questionView.setTextSize(16);
            questionView.setTypeface(null,Typeface.BOLD);
            questionView.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
            questionView.setText(locQuestionName);
            questionCard.addView(questionView);
        }

        if(locMark!=null){
            quesMark.setText(locMark+" Mark");
        }

        if(selectedIndex!=-1){
            quesIndex.setText("#"+(selectedIndex+1));
        }

        if(locImages.length>=1){
            imageView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            Glide.with(QuizObjectActivity.this)
                    .load(locImages[0])
                    .fitCenter()
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder)
                    .crossFade()
                    .dontAnimate()
                    .into(imageView);
            questionCard.addView(imageView);
        }

        if(!locHasMultipleAnswer){
            final RadioButton[] radioButtons = new RadioButton[locChoices.length];
            for(int i=0; i<locChoices.length; i++){
                radioButtons[i]  = new RadioButton(QuizObjectActivity.this);
                radioButtons[i].setText(locChoices[i]);
                radioButtons[i].setId(i+1);
                radioGroup.addView(radioButtons[i]);
            }
            questionCard.addView(radioGroup);
        }else{
            Toast.makeText(QuizObjectActivity.this,"You can check/tick up to "+locMaxAnswer+" checkboxes", Toast.LENGTH_SHORT).show();
            for(int i=0; i<locChoices.length; i++){
                CheckBox checkBox = new CheckBox(QuizObjectActivity.this);
                checkBox.setText(locChoices[i]);
                checkBox.setId(i+1);
                questionCard.addView(checkBox);
            }
        }

    }

    public void storeQuizResumeDetails(final List<QuestionData> qdList,final String objId,final int pc){
        List<String> qIds = new ArrayList<>();

        for(int i=0;i<qdList.size();i++){
            qIds.add(qdList.get(i).getId());
        }

        RestClient.get(QuizObjectActivity.this).storeQuizResumeDetails(qIds, authCode, objId, ""+pc, new Callback<PostStoreDataResponse>() {
            @Override
            public void success(PostStoreDataResponse postStoreDataResponse, Response response) {
                //success
                if(postStoreDataResponse.getData().getStatus().equals("success")){
                    dialog.hide();
                    dialog.dismiss();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (dialog != null && dialog.isShowing()) {
                    dialog.hide();
                    dialog.dismiss();
                }
            }
        });
    }

    public void submitAnswer(List<String> answer,String startTime,String startIndex,String question){
        dialog.show();
        RestClient.get(QuizObjectActivity.this).submitAnswer(answer, startTime, startIndex, question, authCode, ""+practiceCount, new Callback<PostStoreDataResponse>() {
            @Override
            public void success(PostStoreDataResponse postStoreDataResponse, Response response) {
                if(postStoreDataResponse.getData().getStatus().equals("success")){
                    nextQuestion();
                    dialog.hide();
                    dialog.dismiss();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(QuizObjectActivity.this,"Unable to submit answer", Toast.LENGTH_SHORT).show();
                if (dialog != null && dialog.isShowing()) {
                    dialog.hide();
                    dialog.dismiss();
                }
            }
        });
    }

    public void nextQuestion(){
        if (selectedIndex + 1 < questionData.size()) {
            selectedIndex = selectedIndex + 1;
            selectedQuestion = questionData.get(selectedIndex);
            initQuizForm();
            quizProgress.setProgress((int)Math.ceil((selectedIndex*100/questionData.size())));
        } else {
            Toast.makeText(QuizObjectActivity.this,"Thank you for completing the quiz !!!", Toast.LENGTH_SHORT).show();
            createReport(true);
        }
    }

    public void createReport(final boolean reportFlag){
        int startTime = parseToSeconds(quizTimerText.getText().toString());
        int timeTaken = (Integer.parseInt(quizData.getDuration()) * 60) - startTime;

        if(quizTimer!=null){
            quizTimer.cancel();
            quizTimer=null;
        }

        RestClient.get(QuizObjectActivity.this).createReport( objId, ""+timeTaken, authCode, ""+practiceCount, new Callback<CreateReportDataResponse>() {
            @Override
            public void success(CreateReportDataResponse createReportDataResponse, Response response) {
                CreateReportData createReportData = createReportDataResponse.data;
                Log.e("Qz-createReport", createReportData.toString());
                if(reportFlag){
                    hideQuiz();
                    getReport();
                }else{
                    hideQuiz();
                    onBackPressed();
                }
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    public void getReport(){
        dialog.show();
        RestClient.get(QuizObjectActivity.this).getReport(authCode, objId, new Callback<GetReportDataResponse>() {
            @Override
            public void success(GetReportDataResponse getReportDataResponse, Response response) {
                dialog.hide();
                dialog.dismiss();
                List<GetReportData> getReportData = getReportDataResponse.data;
                Log.e("Qz-GetReport", getReportData.toString());

                if(!Boolean.parseBoolean(quizData.quizType)){
                    showQuizReportOneTime();
                    renderOneTimeReport(getReportData.get(0));
                }else{
                    showQuizReportPractice();
                    renderPracticeReport(getReportData);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("Qz-GetReport",error.toString());
                GiveErrorSortIt.SortError(error, QuizObjectActivity.this);
                if (dialog != null && dialog.isShowing()) {
                    dialog.hide();
                    dialog.dismiss();
                }
            }
        });
    }

    public void renderOneTimeReport(final GetReportData data){
        TextView rptNegativeMarkLbl = (TextView) findViewById(R.id.rptNegativeMarkLbl);
        TextView rptReducedLbl = (TextView) findViewById(R.id.rptReducedLbl);

        TextView rptObtained = (TextView) findViewById(R.id.rptObtained);
        TextView rptIncorrect = (TextView) findViewById(R.id.rptIncorrect);
        TextView rptNegativeMark = (TextView) findViewById(R.id.rptNegativeMark);
        TextView rptReduced = (TextView) findViewById(R.id.rptReduced);
        TextView rptFinalMarks = (TextView) findViewById(R.id.rptFinalMarks);
        TextView rptTimeTaken = (TextView) findViewById(R.id.rptTimeTaken);
        TextView rptTotalMarks = (TextView) findViewById(R.id.rptTotalMarks);
        TextView rptTotalQuestions = (TextView) findViewById(R.id.rptTotalQuestions);

        ProgressBar rptPercentageCircle = (ProgressBar) findViewById(R.id.rptPercentageCircle);
        TextView percentageProgressText = (TextView) findViewById(R.id.percentageProgressText);

        List<ResultData> resultData = data.getResult();
        ListView listView = (ListView)findViewById(R.id.result_list);

        ResultAdapter adapter = new ResultAdapter(resultData,getApplicationContext());
        listView.setAdapter(adapter);

        String NegativeMark = data.getNegativeMark();
        String Reduced = data.getReduced();
        String TimeTaken = data.getTimeTaken();
        String percentage = data.getPercentage();
        int progressInt = (int)Math.floor(Double.parseDouble(percentage));

        rptObtained.setText(data.getCorrect());
        rptIncorrect.setText(data.getInCorrectCount());

        if(Double.parseDouble(NegativeMark)<=0){
            rptNegativeMarkLbl.setVisibility(View.GONE);
            rptReducedLbl.setVisibility(View.GONE);
            rptNegativeMark.setVisibility(View.GONE);
            rptReduced.setVisibility(View.GONE);
        }else{
            rptNegativeMark.setText(NegativeMark);
            rptReduced.setText(Reduced);
        }

        if(Integer.parseInt(TimeTaken)<60){
            rptTimeTaken.setText(TimeTaken+" sec");
        }else{
            int min = (int)Math.floor(Double.parseDouble(TimeTaken)/60);
            rptTimeTaken.setText(min+" min");
        }

        rptFinalMarks.setText(data.getFinalMark());
        rptTotalMarks.setText(data.getTotalMarks());
        rptTotalQuestions.setText(""+data.getResult().size());

        if(progressInt<0){
            percentageProgressText.setText("0.00%");
            rptPercentageCircle.setProgress(0);
        }else{
            percentageProgressText.setText(percentage+"%");
            rptPercentageCircle.setProgress(progressInt);
        }
    }

    public void renderPracticeReport(final List<GetReportData> data){
        ExpandableListView expandableListView;
        ExpandableListAdapter expandableListAdapter;
        expandableListView = (ExpandableListView) findViewById(R.id.expandable_list);

        HashMap<String, List<ResultData>> expandableListDetail = new HashMap<>();
        List<String> attempt = new ArrayList<>();

        for(int i=0;i<data.size();i++){
            List<ResultData> result = new ArrayList<>();
            for(int j=0;j<data.get(i).getResult().size();j++){
                result.add(data.get(i).getResult().get(j));
            }
            expandableListDetail.put(data.get(i).get_id(), result);
            attempt.add(data.get(i).get_id());
        }

        expandableListAdapter = new ExpandableListAdapter(this, attempt, expandableListDetail, data);
        expandableListView.setAdapter(expandableListAdapter);
    }

    public int parseToSeconds(String timeStr){
        int result,sec,hr,min;
        hr = Integer.parseInt(timeStr.substring(0,2))*60*60;
        min = Integer.parseInt(timeStr.substring(4,6))*60;
        sec = Integer.parseInt(timeStr.substring(8,10));
        result = sec + min + hr;
        return result;
    }

    public void cancelQuiz(){
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(QuizObjectActivity.this);
        alertDialog.setTitle("Exit Quiz");
        alertDialog.setCancelable(false);
        alertDialog.setMessage("Are you sure?");
        alertDialog.setPositiveButton("Quit",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        createReport(false);
                    }
                });
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                dialog.dismiss();
            }
        });
        alertDialog.show();
    }

    public void showQuiz(){
        quizCardView.setVisibility(View.VISIBLE);
        playCardView.setVisibility(View.GONE);
        alreadyTakenCardView.setVisibility(View.GONE);
    }

    public void showPlay(){
        playCardView.setVisibility(View.VISIBLE);
        quizCardView.setVisibility(View.GONE);
        alreadyTakenCardView.setVisibility(View.GONE);
    }

    public void showAlreadyTaken(final int flag){
        if (dialog != null && dialog.isShowing()) {
            dialog.hide();
            dialog.dismiss();
        }
        if(flag==0){
            alreadyTakenOnetime.setVisibility(View.VISIBLE);
            alreadyTakenPractice.setVisibility(View.GONE);
        }else{
            alreadyTakenPractice.setVisibility(View.VISIBLE);
            alreadyTakenOnetime.setVisibility(View.GONE);
        }
        alreadyTakenCardView.setVisibility(View.VISIBLE);
        playCardView.setVisibility(View.GONE);
        quizCardView.setVisibility(View.GONE);
    }

    public void showEmpty(){
        emptyText.setVisibility(View.VISIBLE);
        playCardView.setVisibility(View.GONE);
        quizCardView.setVisibility(View.GONE);
        alreadyTakenCardView.setVisibility(View.GONE);
    }

    public void showQuizReportOneTime() {
        quizReportOneTime.setVisibility(View.VISIBLE);
        emptyText.setVisibility(View.GONE);
        playCardView.setVisibility(View.GONE);
        quizCardView.setVisibility(View.GONE);
        alreadyTakenCardView.setVisibility(View.GONE);
    }

    public void showQuizReportPractice() {
        quizReportPractice.setVisibility(View.VISIBLE);
        quizReportOneTime.setVisibility(View.GONE);
        emptyText.setVisibility(View.GONE);
        playCardView.setVisibility(View.GONE);
        quizCardView.setVisibility(View.GONE);
        alreadyTakenCardView.setVisibility(View.GONE);
    }

    public void hideQuiz(){
        quizCardView.setVisibility(View.GONE);
    }

    public void hidePlay(){
        playCardView.setVisibility(View.GONE);
    }

    public void hideAlreadyTaken(){
        alreadyTakenCardView.setVisibility(View.GONE);
    }

    public void hideQuizReportOneTime(){
        quizReportOneTime.setVisibility(View.GONE);
    }

    public void hideQuizReportPractice(){
        quizReportPractice.setVisibility(View.GONE);
    }

    @Override
    public void onBackPressed() {
        if(quizTimer==null){
            super.onBackPressed();
        }else{
            cancelQuiz();
        }
    }

    public class QuizTimer extends CountDownTimer {

        public QuizTimer(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            long hr = TimeUnit.MILLISECONDS.toHours(millisUntilFinished);
            long min = TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millisUntilFinished));
            long sec = TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished));
            String timeLeft = String.format("%02d"+"h:"+"%02d"+"m:"+"%02d"+"s", hr, min, sec);
            quizTimerText.setText(timeLeft);

            if(timeLeft.equals("00h:01m:00s")){
                Toast.makeText(QuizObjectActivity.this,"last 1 minute left", Toast.LENGTH_SHORT).show();
            }

            if(timeLeft.equals("00h:03m:00s")){
                Toast.makeText(QuizObjectActivity.this,"3 more minutes left", Toast.LENGTH_SHORT).show();
            }

            if(timeLeft.equals("00h:05m:00s")){
                Toast.makeText(QuizObjectActivity.this,"5 more minutes left", Toast.LENGTH_SHORT).show();
            }

            if(timeLeft.equals("00h:10m:00s")){
                Toast.makeText(QuizObjectActivity.this,"10 more minutes left", Toast.LENGTH_SHORT).show();
            }

            if(timeLeft.equals("00h:15m:00s")){
                Toast.makeText(QuizObjectActivity.this,"15 more minutes left", Toast.LENGTH_SHORT).show();
            }

            if(timeLeft.equals("00h:20m:00s")){
                Toast.makeText(QuizObjectActivity.this,"20 more minutes left", Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onFinish() {
            Toast.makeText(QuizObjectActivity.this,"Times Up !!!", Toast.LENGTH_SHORT).show();
            hideQuiz();
            createReport(true);
        }
    }

}
