package in.zapprep.ZapPrep;

import java.util.ArrayList;

/**
 * Created by PoolDead on 12/25/2017.
 */

public class InvitePageResponse {
    public String more;
    public ArrayList<InviteData> data;

    public String getMore() {
        return more;
    }

    public void setMore(String more) {
        this.more = more;
    }

    public ArrayList<InviteData> getData() {
        return data;
    }

}
