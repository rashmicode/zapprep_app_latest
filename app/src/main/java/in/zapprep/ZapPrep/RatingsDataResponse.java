package in.zapprep.ZapPrep;

import java.util.List;

/**
 * Created by Tushar on 9/14/2015.
 */
public class RatingsDataResponse {


    public String allowPublish;
    List<RatingsData> data;
    public String more;

    public String getAllowPublish() {
        return allowPublish;
    }

    public void setAllowPublish(String allowPublish) {
        this.allowPublish = allowPublish;
    }

    public List<RatingsData> getData() {
        return data;
    }

    public void setData(List<RatingsData> data) {
        this.data = data;
    }

    public String getMore() {
        return more;
    }

    public void setMore(String more) {
        this.more = more;
    }
}
