package in.zapprep.ZapPrep;

/**
 * Created by PoolDead on 7/3/2018.
 */

public class PracticeCountDataResponse {
    PracticeCountData data;

    public PracticeCountData getData() {
        return data;
    }

    public void setData(PracticeCountData data) {
        this.data = data;
    }
}
