package in.zapprep.ZapPrep;

/**
 * Created by PoolDead on 7/3/2018.
 */

public class PracticeCountData {
    String practiceCount;
    String status;

    public String getPracticeCount() {
        return practiceCount;
    }

    public void setPracticeCount(String practiceCount) {
        this.practiceCount = practiceCount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "PracticeCountData{" +
                "practiceCount='" + practiceCount + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
