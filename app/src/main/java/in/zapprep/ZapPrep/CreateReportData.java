package in.zapprep.ZapPrep;

import java.util.List;

/**
 * Created by PoolDead on 7/6/2018.
 */

public class CreateReportData {
    public String practiceCount;
    public String timeTaken;
    public String percentage;
    public String correct;
    public String reduced;
    public String inCorrectCount;
    public String totalMarks;
    public String negativeMark;
    public String finalMark;
    public List<String> questions;
    public String object;
    public String path;
    public String concept;
    public String producer;
    public String user;

    public String getPracticeCount() {
        return practiceCount;
    }

    public void setPracticeCount(String practiceCount) {
        this.practiceCount = practiceCount;
    }

    public String getTimeTaken() {
        return timeTaken;
    }

    public void setTimeTaken(String timeTaken) {
        this.timeTaken = timeTaken;
    }

    public String getPercentage() {
        return percentage;
    }

    public void setPercentage(String percentage) {
        this.percentage = percentage;
    }

    public String getCorrect() {
        return correct;
    }

    public void setCorrect(String correct) {
        this.correct = correct;
    }

    public String getReduced() {
        return reduced;
    }

    public void setReduced(String reduced) {
        this.reduced = reduced;
    }

    public String getInCorrectCount() {
        return inCorrectCount;
    }

    public void setInCorrectCount(String inCorrectCount) {
        this.inCorrectCount = inCorrectCount;
    }

    public String getTotalMarks() {
        return totalMarks;
    }

    public void setTotalMarks(String totalMarks) {
        this.totalMarks = totalMarks;
    }

    public String getNegativeMark() {
        return negativeMark;
    }

    public void setNegativeMark(String negativeMark) {
        this.negativeMark = negativeMark;
    }

    public String getFinalMark() {
        return finalMark;
    }

    public void setFinalMark(String finalMark) {
        this.finalMark = finalMark;
    }

    public List<String> getQuestions() {
        return questions;
    }

    public void setQuestions(List<String> questions) {
        this.questions = questions;
    }

    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getConcept() {
        return concept;
    }

    public void setConcept(String concept) {
        this.concept = concept;
    }

    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "CreateReportData{" +
                "practiceCount='" + practiceCount + '\'' +
                ", timeTaken='" + timeTaken + '\'' +
                ", percentage='" + percentage + '\'' +
                ", correct='" + correct + '\'' +
                ", reduced='" + reduced + '\'' +
                ", inCorrectCount='" + inCorrectCount + '\'' +
                ", totalMarks='" + totalMarks + '\'' +
                ", negativeMark='" + negativeMark + '\'' +
                ", finalMark='" + finalMark + '\'' +
                ", questions=" + questions +
                ", object='" + object + '\'' +
                ", path='" + path + '\'' +
                ", concept='" + concept + '\'' +
                ", producer='" + producer + '\'' +
                ", user='" + user + '\'' +
                '}';
    }
}
