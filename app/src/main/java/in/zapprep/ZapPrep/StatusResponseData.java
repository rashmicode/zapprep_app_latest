package in.zapprep.ZapPrep;

/**
 * Created by PoolDead on 11/24/2017.
 * Mentor Online Status
 */

class StatusResponseData {
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
