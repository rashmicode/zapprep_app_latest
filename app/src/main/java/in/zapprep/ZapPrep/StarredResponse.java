package in.zapprep.ZapPrep;

import java.util.List;

/**
 * Created by Tushar on 7/17/2015.
 */
public class StarredResponse {

    public List<StarredChatSenderData> data;

    public List<StarredChatSenderData> getData() {
        return data;
    }

    public void setData(List<StarredChatSenderData> data) {
        this.data = data;
    }
}
