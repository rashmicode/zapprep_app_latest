package in.zapprep.ZapPrep;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


/**
 * A placeholder fragment containing a simple view.
 */
public class PeoplePageActivityFragment extends Fragment {
    PeoplePageAdapter peoplePageAdapter;
    public List<PeopleData> list;
    ProgressDialog dialog;
    String mPathID;
    String mFlag;

    String page;


    public PeoplePageActivityFragment() {
    }

    public static PeoplePageActivityFragment newInstance(String pathID, String flag) {
        PeoplePageActivityFragment fragment = new PeoplePageActivityFragment();
        Bundle args = new Bundle();
        args.putString("pathID", pathID);
        args.putString("flag", flag);
        fragment.setArguments(args);
        return fragment;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mPathID = getArguments().getString("pathID");
            mFlag = getArguments().getString("flag");
            Log.e("", "PathId in people page" + mPathID);
        }
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_people_page, container, false);
        final ListView peopleDataList = (ListView) rootView.findViewById(R.id.people_page_list);

        list = new ArrayList<>();
        if (mFlag.equals("ExploreCourseRedirect")) {
            peoplePageAdapter = new PeoplePageAdapter(getActivity(), list, "ExploreCourseRedirect");
        } else {
            peoplePageAdapter = new PeoplePageAdapter(getActivity(), list, "Clickable");
        }
        peopleDataList.setAdapter(peoplePageAdapter);
        dialog = CustomProgressDialog.getCustomProgressDialog(getActivity());
        dialog.show();
        page = "1";
        getValuesFromBackend();
        setHasOptionsMenu(true);
        return rootView;
    }

    public void getValuesFromBackend() {
        RestClient.get(getActivity()).getPeoplePageData(page, mPathID, new Callback<PeoplePageResponse>() {
            @Override
            public void success(PeoplePageResponse peoplePageResponse, Response response) {
                Log.i("", "SuccessMessage" + peoplePageResponse.toString());
                // success!

                list.addAll(peoplePageResponse.getData());
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        peoplePageAdapter.notifyDataSetChanged();
                    }
                });
                dialog.hide();
                dialog.dismiss();
              /*  if (peoplePageResponse.getMore().equals("true")) {
                    int pageNumber = Integer.parseInt(page);
                    pageNumber++;
                    page = String.valueOf(pageNumber);
                    getValuesFromBackend();


                } else {
                    peoplePageAdapter.hideProgressBar();
                }*/
            }

            @Override
            public void failure(RetrofitError error) {
                dialog.hide();
                dialog.dismiss();
                try {
                    if (getActivity().isFinishing()) {
                        getActivity().finish();
                    } else {
                        GiveErrorSortIt.SortError(error, getActivity());
                    }
                } catch (Exception e) {
                    Toast.makeText(getActivity().getApplicationContext(),"Some error occured",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        if (!mFlag.equals("ExploreCourseRedirect")) {
            MenuInflater menuInflater = getActivity().getMenuInflater();
            menuInflater.inflate(R.menu.menu_people_page, menu);


            SearchManager searchManager =
                    (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
            SearchView searchView =
                    (SearchView) menu.findItem(R.id.action_search).getActionView();
            searchView.setSearchableInfo(
                    searchManager.getSearchableInfo(getActivity().getComponentName()));

            SearchView.OnQueryTextListener textChangeListener = new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextChange(String newText) {
                    // this is your adapter that will be filtered
                    peoplePageAdapter.getFilter().filter(newText);
                    Log.e("on text chnge text: ", "" + newText);
                    return false;
                }

                @Override
                public boolean onQueryTextSubmit(String query) {
                    // this is your adapter that will be filtered
                    Log.e("on query submit: ", "" + query);
                    peoplePageAdapter.getFilter().filter(query);

                    return true;
                }
            };
            searchView.setOnQueryTextListener(textChangeListener);

        }
    }


}
