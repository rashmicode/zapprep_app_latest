package in.zapprep.ZapPrep;

import java.util.List;

import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Multipart;
import retrofit.http.PATCH;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.http.Query;
import retrofit.mime.TypedFile;
import retrofit.mime.TypedString;

/**
 * Created by Tushar on 5/14/2015.
 */
public interface Api {
    //MarketPlace
    @GET("/paths/home")
    public void getMarketPlaceData(@Query("access_token") String access_token,Callback<MarketPlaceResponseData> callback);

    //MarketPlace
    @GET("/paths/v2")
    public void getMarketPlaceCategoryData(@Query("access_token") String access_token,@Query("cat") String cat,Callback<MarketPlaceResponseData> callback);

    //MarketPlace
    @GET("/paths/v2")
    public void getMarketPlaceSubCategoryData(@Query("access_token") String access_token,@Query("cat") String cat,@Query("subCat") String subCat,Callback<MarketPlaceResponseData> callback);

    //MarketPlace
    @GET("/paths/search?limit=100&page=1")
    public void getSearchResults(@Query("access_token") String access_token,@Query("query") String query,Callback<MarketPlaceResponseData> callback);

    //ExploreCourse
    @GET("/paths?limit=100&page=1")
    public void getExploreCourseData(@Query("access_token") String access_token,@Query("pathId") String pathId,Callback<ExploreCourseResponseData> callback);

    //DashBoard
    @GET("/user/paths?limit=100&page=1")
    public void getDashBoardData(@Query("access_token") String access_token, Callback<DashBoardResponseData> callback);

    //SignUP
    @FormUrlEncoded
    @POST("/user/signup")
    public void postUserData(@Field("username") String username, @Field("email") String email, @Field("password") String password, @Field("name") String name, @Field("phone") String phone, Callback<SignUpResponseData> callback);

    //Login
    @FormUrlEncoded
    @POST("/user/login")
    public void postLoginData(@Field("email") String email, @Field("password") String password, Callback<LoginResponseData> callback);

    //Update Online Status(Active)
    @FormUrlEncoded
    @POST("/user/osactive")
    public void postOnlineStatusActive(@Field("access_token") String access_token, Callback<StatusResponseData> callback);

    //Update Online Status(Inactive)
    @FormUrlEncoded
    @POST("/user/osinactive")
    public void postOnlineStatusInActive(@Field("access_token") String access_token, Callback<StatusResponseData> callback);

    //ConsumeCourse_CoursePage
    @GET("/concepts")
    public void getConsumeCoursePage(@Query("access_token") String access_token, @Query("pathId") String pathId, Callback<ConceptResponse> callBack);

    //Getting Vidyo_Token for video chat
    @GET("/concepts/getVidyoToken")
    public void getVidyoToken(@Query("access_token") String access_token, Callback<VidyoTokenResponseData> callBack);

    //ProfilePage
    @GET("/user")
    public void getProfileResponseData(@Query("userId") String userId, Callback<ProfileResponse> callBack);

    //PatchProfileImageDescTagLine
    @Multipart
    @PATCH("/user")
    public void postPatchProfilePhoto(@Query("access_token") String authcode, @Part("avatar") TypedFile avatar,@Part("userDesc") TypedString userDesc, @Part("tagline") TypedString tagLIne, Callback<ProfilePatchResponse> callback);

    //PatchChangePassword
    @FormUrlEncoded
    @PATCH("/user/password")
    public void changePassword(@Query("access_token") String authcode,@Field("currPass") String oldPassword,@Field("newPass") String newPassword,@Field("rePass") String rePassword, Callback<ProfilePatchResponse> callback);

    //PatchGCMToken
    @Multipart
    @PATCH("/user")
    public void postPatchGCMToken(@Query("access_token") String authcode, @Part("gcmId") TypedString gcmToken, Callback<ProfilePatchResponse> callback);


    @FormUrlEncoded
    @POST("/user/enroll")
    public void postUserEnrollCourse(@Query("access_token") String access_token, @Field("pathId") String pathId,@Field("paymentId") String paymentId,@Field("amount") String amount, Callback<Object> callback);

    //ForgotPassword
    @FormUrlEncoded
    @POST("/user/forgotpassword")
    public void forgotPasswordMail(@Field("email") String email, Callback<Object> callback);


    @GET("/paths/codes")
    public void postUserAccessCode(@Query("access_token") String access_token, @Query("accessCode") String accessId, Callback<CodeResponseData> callback);


    @GET("/paths/codes")
    public void postUserPromoCode(@Query("access_token") String access_token, @Query("promoCode") String accessId,@Query("mPathID") String mPathID, Callback<CodeResponseData> callback);

    //Get search auto suggest list
    @GET("/paths/suggest")
    public void getSearchData(@Query("query") String query, Callback<AutoSuggest> callback);

    //PeoplePage
    @GET("/paths/users")
    public void getPeoplePageData(@Query("page") String page,@Query("pathId") String pathId, Callback<PeoplePageResponse> callBack);

    //InvitePage
    @GET("/invite/getInvites")
    public void getInviteData(@Query("page") String page,@Query("pathId") String pathId, Callback<InvitePageResponse> callBack);

    //InvitePage - Invite People
    @FormUrlEncoded
    @POST("/invite/")
    public void postInvite(@Field("uid") String uid,@Field("pathId") String pathId, Callback<InviteResponse> callBack);

    //InvitePage - cancel Invite
    @FormUrlEncoded
    @POST("/invite/cancelInvite")
    public void postCancelInvite(@Field("uid") String uid,@Field("pathId") String pathId, Callback<InviteResponse> callBack);

    //InvitePage - Accept Invite
    @FormUrlEncoded
    @POST("/invite/acceptInvite")
    public void postAcceptInvite(@Field("uid") String uid,@Field("pathId") String pathId, Callback<InviteAcceptResponse> callBack);

    //Announcements
    @GET("/announcements")
    public void getAnnouncements(@Query("access_token") String access_token, @Query("pathId") String pathId, @Query("limit") String limit,@Query("page") String page,Callback<AnnouncementResponse> callBack);

    //PostAnnouncements
    @FormUrlEncoded
    @POST("/announcements")
    public void postAnnouncements(@Query("access_token") String access_token, @Field("pathId") String pathID, @Field("desc") String desc, @Field("title") String title, Callback<AnnouncementPostResponse> callBack);

    //Starred
    @GET("/chat/star")
    public void getStarred(@Query("access_token") String access_token, @Query("pathID") String pathID, Callback<StarredResponse> callBack);


    //PostStarredFromChat
    @FormUrlEncoded
    @POST("/chat/star")
    public void postStarredMessage(@Query("access_token") String access_token, @Field("text") String text
            , @Field("senderQbID") String senderQbId, @Field("pathID") String pathID, @Field("roomId") String roomId,
                                   @Field("roomName") String roomName, @Field("timestamp") String timestamp,
                                   @Field("messageId") String messageId,
                                   Callback<StarredResponse> callBack);

    //PostErrorStack
    @Multipart
    @POST("/user/error")
    public void postErrorStack(@Query("access_token") String access_token, @Part("dump") TypedString error,Callback<PeoplePageResponse> callBack);

    //PostGoogleAuthCode
    @FormUrlEncoded
    @POST("/user/auth/google")
    public void postGoogleAuthCode(@Field("code") String code,Callback<LoginResponseData> callBack);

    //PostFacebookAuthCode
    @FormUrlEncoded
    @POST("/user/auth/facebook")
    public void postFacebookAuthCode(@Field("code") String code,Callback<LoginResponseData> callBack);

    //CourseRating
    @GET("/reviews?limit=100&page=1")
    public void getCourseRating(@Query("access_token") String access_token, @Query("pathId") String pathId, Callback<RatingsDataResponse> callBack);

    //CourseRating
    @FormUrlEncoded
    @POST("/reviews")
    public void postCourseRating(@Query("access_token") String access_token, @Field("review") String review
            , @Field("rating") String rating, @Field("title") String title, @Field("pathId") String pathId,Callback<RatingsPostResponse> callBack);

    //CourseRating
    @FormUrlEncoded
    @PATCH("/reviews")
    public void patchCourseRating(@Query("access_token") String access_token, @Field("review") String review
            , @Field("rating") String rating, @Field("title") String title, @Field("id") String reviewId, @Field("pathId") String pathId,Callback<RatingsPostResponse> callBack);

    //PatchUserName
    @Multipart
    @PATCH("/user")
    public void patchUserName(@Query("access_token") String authcode, @Part("username") TypedString username,@Part("email") TypedString email, Callback<ProfilePatchResponse> callback);

    //CourseRating
    @FormUrlEncoded
    @POST("/payments")
    public void postPayment(@Query("access_token") String access_token, @Field("paymentId") String paymentId
            , @Field("pathId") String pathId, @Field("currency") String currency,@Field("discountCode") String promoCode, @Field("amount") String amount,@Field("product") String product,Callback<PaymentResponse> callBack);

    //User Transaction History
    @GET("/payments")
    public void getUserTransactionHistory(@Query("access_token") String access_token, Callback<TransactionResponse> callBack);

    //Mentor Transaction History
    @GET("/payments/mentor")
    public void getMentorTransactionHistory(@Query("access_token") String access_token, Callback<TransactionResponse> callBack);

    //Quiz Related Endpoints
    //GET Quiz Details
    @GET("/objects/getQuiz")
    public void getQuizData(@Query("access_token") String access_token,@Query("objectId") String objectId, Callback<QuizDataResponse> callBack);

    //GET Practice Count
    @GET("/objects/getPracticeCount")
    public void getPracticeCount(@Query("access_token") String access_token,@Query("objectId") String objectId,@Query("practiceCount") String practiceCount, Callback<PracticeCountDataResponse> callBack);

    //GET get Quiz Resume Status
    @GET("/objects/getQuizResumeStatus")
    public void getQuizResumeStatus(@Query("access_token") String access_token,@Query("objectId") String objectId,@Query("practiceCount") String practiceCount, Callback<ResumeStatusDataResponse> callBack);

    //GET get Questions
    @GET("/objects/getQuestions")
    public void getQuestions(@Query("access_token") String access_token,@Query("objectId") String objectId, Callback<QuestionDataResponse> callBack);

    //Store Quiz Resume Details
    @FormUrlEncoded
    @POST("/objects/storeQuizResumeDetails")
    public void storeQuizResumeDetails(@Field("questions") List<String> questions, @Field("access_token") String access_token, @Field("objectId") String objectId, @Field("practiceCount") String practiceCount, Callback<PostStoreDataResponse> callBack);

    //Store Quiz Answer
    @FormUrlEncoded
    @POST("/objects/submitAnswer")
    public void submitAnswer(@Field("answer") List<String> answer,@Field("startTime") String startTime,@Field("startIndex") String startIndex, @Field("question") String question, @Field("access_token") String access_token, @Field("practiceCount") String practiceCount, Callback<PostStoreDataResponse> callBack);

    //Create Quiz Report
    @FormUrlEncoded
    @POST("/objects/createReport")
    public void createReport(@Field("objectId") String objectId,@Field("timeTaken") String timeTaken, @Field("access_token") String access_token, @Field("practiceCount") String practiceCount, Callback<CreateReportDataResponse> callBack);

    //Get Quiz Report
    @GET("/objects/getReport")
    public void getReport(@Query("access_token") String access_token,@Query("objectId") String objectId, Callback<GetReportDataResponse> callBack);

}