package in.zapprep.ZapPrep;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by PoolDead on 7/8/2018.
 */

public class ResultAdapter extends ArrayAdapter<ResultData>{

    private List<ResultData> resultData;
    Context mContext;

    // View lookup cache
    private static class ViewHolder {
        TextView txtName;
        TextView txtChoices;
        TextView txtYourAnswer;
        TextView txtMark;
    }

    public ResultAdapter(List<ResultData> data, Context context) {
        super(context, R.layout.result_list, data);
        this.resultData = data;
        this.mContext=context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        ResultData result = getItem(position);
        ViewHolder viewHolder;
        String choices;
        String yourAnswer;

        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.result_list, parent, false);
            viewHolder.txtName = (TextView) convertView.findViewById(R.id.name);
            viewHolder.txtChoices = (TextView) convertView.findViewById(R.id.choices);
            viewHolder.txtYourAnswer = (TextView) convertView.findViewById(R.id.your_answer);
            viewHolder.txtMark = (TextView) convertView.findViewById(R.id.mark);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        choices = "Choices :-\n";
        for(int i=0;i<result.getChoices().size();i++){
            choices += "\t"+(i+1)+"). "+result.getChoices().get(i)+"\n";
        }

        yourAnswer = "Your Answer(s) :-\n";
        for(int j=0;j<result.getYourAnswer().size();j++){
            yourAnswer += "\t"+(j+1)+"). "+result.getYourAnswer().get(j)+"\n";
        }

        viewHolder.txtName.setText((position+1)+"). "+result.getName());
        viewHolder.txtChoices.setText(choices);
        viewHolder.txtYourAnswer.setText(yourAnswer);
        // Return the completed view to render on screen

        if(result.getStatus().equals("correct")){
            viewHolder.txtMark.setText("Marks :- "+result.getMark());
            convertView.setBackgroundResource(R.color.correct);
        }else if(result.getStatus().equals("incorrect")){
            viewHolder.txtMark.setText("Marks :- 0");
            convertView.setBackgroundResource(R.color.incorrect);
        }else if(result.getStatus().equals("notAttempted")){
            viewHolder.txtMark.setText("Marks :- 0");
            convertView.setBackgroundResource(R.color.notAttempted);
        }

        return convertView;
    }
}