package in.zapprep.ZapPrep;

import java.util.List;

/**
 * Created by Dell on 11/27/2015.
 */
public class TransactionResponse {

    public List<TransactionData> data;

    public List<TransactionData> getData() {
        return data;
    }

    public void setData(List<TransactionData> data) {
        this.data = data;
    }
}
