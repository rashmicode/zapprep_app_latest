package in.zapprep.ZapPrep.Notifications;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import in.zapprep.ZapPrep.LandingPageActivity;
import in.zapprep.ZapPrep.R;

import java.util.Random;


public class GCMUtils {


    public static String GROUP_KEY_EMAILS;
    public static int randomNumber;

    @TargetApi(20)
    public static void generateNotification(Context context, String message, String title,String link,String pathId,String positionString) {
        long when = System.currentTimeMillis();
        NotificationManager notificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);
        Log.e("PushNotification","The push notification received is"+title);
        int max=1000000;
        int min=0;
        Random random = new Random();
        randomNumber = random.nextInt(max - min) + min;
        aggregateNotifications(title);

        Notification noti = new Notification.Builder(context)
                .setContentTitle("ZapPrep: "+title)
                .setContentText(message)
                .setSmallIcon(getNotificationIcon())
                .setGroup(GROUP_KEY_EMAILS)
                .setStyle(new Notification.BigTextStyle()
                        .bigText(message))
                .build();
        //String title = context.getString(R.string.app_name);

        Intent notificationIntent = new Intent(context, LandingPageActivity.class);
        notificationIntent.putExtra("link",link );
        notificationIntent.putExtra("title",title );
        notificationIntent.putExtra("pathID",pathId );
        notificationIntent.putExtra("position",positionString);
        Log.e("","Values passed"+link+title+pathId);


        //Add new task clear top
        PendingIntent intent = PendingIntent.getActivity(context, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        noti.setLatestEventInfo(context, "ZapPrep", message, intent);
        noti.flags |= Notification.FLAG_AUTO_CANCEL;

        // Play default notification sound
        noti.defaults |= Notification.DEFAULT_SOUND;

        // Vibrate if vibrate is enabled
        noti.defaults |= Notification.DEFAULT_VIBRATE;

        Log.e("", "Notification number"+randomNumber);
        notificationManager.notify(randomNumber, noti);

    }
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public static void generateNotificationForRemaining(Context context, String message, String title,String link,String pathId,String positionString) {
        long when = System.currentTimeMillis();
        NotificationManager notificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);

        Notification noti = new Notification.Builder(context)
                .setContentTitle("ZapPrep: " + title)
                .setContentText(message)
                .setSmallIcon(getNotificationIcon())
                .setStyle(new Notification.BigTextStyle()
                        .bigText(message))
                .build();
        //String title = context.getString(R.string.app_name);

        Intent notificationIntent = new Intent(context, LandingPageActivity.class);
        notificationIntent.putExtra("link",link );
        notificationIntent.putExtra("title",title );
        notificationIntent.putExtra("pathID",pathId );
        notificationIntent.putExtra("position",positionString);
        //Add new task clear top
        PendingIntent intent = PendingIntent.getActivity(context, (int) System.currentTimeMillis(), notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        noti.setLatestEventInfo(context, "ZapPrep", message, intent);
        noti.flags |= Notification.FLAG_AUTO_CANCEL;

        // Play default notification sound
        noti.defaults |= Notification.DEFAULT_SOUND;

        // Vibrate if vibrate is enabled
        noti.defaults |= Notification.DEFAULT_VIBRATE;

        int max=1000000;
        int min=0;
        Random random = new Random();
        int randomNumber = random.nextInt(max - min) + min;
        Log.e("", "Notification number"+randomNumber);
        notificationManager.notify(randomNumber, noti);

    }

    public static void aggregateNotifications(String title)
    {
        switch (title) {
            case "New Concept":
                GROUP_KEY_EMAILS="new_concept";
                //randomNumber=1;
                break;
            case "New Content":
                GROUP_KEY_EMAILS="new_content";
                //randomNumber=2;
                break;
            case "New Course":
                GROUP_KEY_EMAILS="new course";
               // randomNumber=3;
                break;
            case "Your Mentor is online":
                GROUP_KEY_EMAILS="mentor_online";
                randomNumber=4;
                break;
            case "Announcement":
                GROUP_KEY_EMAILS="announcement";
                //randomNumber=5;
                break;
            case "Response for your Feedback":
                GROUP_KEY_EMAILS="feedback";
                //randomNumber=5;
                break;
            case "You have been mentioned!":
                GROUP_KEY_EMAILS="mentioned";
                //randomNumber=6;
                break;
            case "New message from Mentor!":
                //GROUP_KEY_EMAILS="mentor_message";
                //randomNumber=7;
                break;
            default:
                GROUP_KEY_EMAILS="default";
                //randomNumber=8;
                break;

        }
    }
    public static int getNotificationIcon() {
        boolean whiteIcon = (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP);
        return whiteIcon ? R.drawable.ic_launcher : R.drawable.ic_launcher;
    }
}
