package in.zapprep.ZapPrep;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.HashMap;
import java.util.List;

import static com.facebook.FacebookSdk.getApplicationContext;


/**
 * Created by PoolDead on 7/9/2018.
 */

public class ExpandableListAdapter extends BaseExpandableListAdapter {
    private Context context;
    private List<GetReportData> data;
    private List<String> attempt;
    private HashMap<String, List<ResultData>> expandableListDetail;

    public ExpandableListAdapter(Context context, List<String> attempt, HashMap<String, List<ResultData>> expandableListDetail,List<GetReportData> data) {
        this.context = context;
        this.data = data;
        this.expandableListDetail = expandableListDetail;
        this.attempt = attempt;
    }

    @Override
    public Object getChild(int listPosition, int expandedListPosition) {
        return this.expandableListDetail.get(this.attempt.get(listPosition)).get(expandedListPosition);
    }

    @Override
    public long getChildId(int listPosition, int expandedListPosition) {
        return expandedListPosition;
    }

    @Override
    public View getChildView(int listPosition, final int expandedListPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {
        ResultData resData = (ResultData) getChild(listPosition, expandedListPosition);
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.expandable_list_inner_item, null);
        }

        TextView txtName = (TextView) convertView.findViewById(R.id.name);
        TextView txtChoices = (TextView) convertView.findViewById(R.id.choices);
        TextView txtYourAnswer = (TextView) convertView.findViewById(R.id.your_answer);
        TextView txtMark = (TextView) convertView.findViewById(R.id.mark);
        String choices;
        String yourAnswer;

        choices = "Choices :-\n";
        for(int i=0;i<resData.getChoices().size();i++){
            choices += "\t"+(i+1)+"). "+resData.getChoices().get(i)+"\n";
        }

        yourAnswer = "Your Answer(s) :-\n";
        for(int j=0;j<resData.getYourAnswer().size();j++){
            yourAnswer += "\t"+(j+1)+"). "+resData.getYourAnswer().get(j)+"\n";
        }

        txtName.setText((expandedListPosition+1)+"). "+resData.getName());
        txtChoices.setText(choices);
        txtYourAnswer.setText(yourAnswer);

        if(resData.getStatus().equals("correct")){
            txtMark.setText("Marks :- "+resData.getMark());
            convertView.setBackgroundResource(R.color.correct);
        }else if(resData.getStatus().equals("incorrect")){
            txtMark.setText("Marks :- 0");
            convertView.setBackgroundResource(R.color.incorrect);
        }else if(resData.getStatus().equals("notAttempted")){
            txtMark.setText("Marks :- 0");
            convertView.setBackgroundResource(R.color.notAttempted);
        }

        return convertView;
    }

    @Override
    public int getChildrenCount(int listPosition) {
        return this.expandableListDetail.get(this.attempt.get(listPosition)).size();
    }

    @Override
    public Object getGroup(int listPosition) {
        return this.data.get(listPosition);
    }

    @Override
    public int getGroupCount() {
        return this.data.size();
    }

    @Override
    public long getGroupId(int listPosition) {
        return listPosition;
    }

    @Override
    public View getGroupView(int listPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        GetReportData dt = (GetReportData) getGroup(listPosition);

        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.expandable_list_item, null);
        }

        TextView rptNegativeMarkLbl = (TextView) convertView.findViewById(R.id.rptNegativeMarkLbl);
        TextView rptReducedLbl = (TextView) convertView.findViewById(R.id.rptReducedLbl);
        TextView attemptTitle = (TextView) convertView.findViewById(R.id.attemptTitle);

        TextView rptObtained = (TextView) convertView.findViewById(R.id.rptObtained);
        TextView rptIncorrect = (TextView) convertView.findViewById(R.id.rptIncorrect);
        TextView rptNegativeMark = (TextView) convertView.findViewById(R.id.rptNegativeMark);
        TextView rptReduced = (TextView) convertView.findViewById(R.id.rptReduced);
        TextView rptFinalMarks = (TextView) convertView.findViewById(R.id.rptFinalMarks);
        TextView rptTimeTaken = (TextView) convertView.findViewById(R.id.rptTimeTaken);
        TextView rptTotalMarks = (TextView) convertView.findViewById(R.id.rptTotalMarks);
        TextView rptTotalQuestions = (TextView) convertView.findViewById(R.id.rptTotalQuestions);

        ProgressBar rptPercentageCircle = (ProgressBar) convertView.findViewById(R.id.rptPercentageCircle);
        TextView percentageProgressText = (TextView) convertView.findViewById(R.id.percentageProgressText);

        String NegativeMark = dt.getNegativeMark();
        String Reduced = dt.getReduced();
        String TimeTaken = dt.getTimeTaken();
        String percentage = dt.getPercentage();
        int progressInt = (int)Math.floor(Double.parseDouble(percentage));

        attemptTitle.setText("Attempt:- #"+dt.getPracticeCount());
        rptObtained.setText(dt.getCorrect());
        rptIncorrect.setText(dt.getInCorrectCount());

        if(Double.parseDouble(NegativeMark)<=0){
            rptNegativeMarkLbl.setVisibility(View.GONE);
            rptReducedLbl.setVisibility(View.GONE);
            rptNegativeMark.setVisibility(View.GONE);
            rptReduced.setVisibility(View.GONE);
        }else{
            rptNegativeMark.setText(NegativeMark);
            rptReduced.setText(Reduced);
        }

        if(Integer.parseInt(TimeTaken)<60){
            rptTimeTaken.setText(TimeTaken+" sec");
        }else{
            int min = (int)Math.floor(Double.parseDouble(TimeTaken)/60);
            rptTimeTaken.setText(min+" min");
        }

        rptFinalMarks.setText(dt.getFinalMark());
        rptTotalMarks.setText(dt.getTotalMarks());
        rptTotalQuestions.setText(""+dt.getResult().size());

        if(progressInt<0){
            percentageProgressText.setText("0.00%");
            rptPercentageCircle.setProgress(0);
        }else{
            percentageProgressText.setText(percentage+"%");
            rptPercentageCircle.setProgress(progressInt);
        }

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int listPosition, int expandedListPosition) {
        return true;
    }
}
