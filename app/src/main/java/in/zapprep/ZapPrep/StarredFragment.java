package in.zapprep.ZapPrep;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;

import in.zapprep.ZapPrep.dummy.DummyContent;

import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * A fragment representing a list of Items.
 * <p>
 * Large screen devices (such as tablets) are supported by replacing the ListView
 * with a GridView.
 * <p>
 * Activities containing this fragment MUST implement the {@link OnFragmentInteractionListener}
 * interface.
 */
public class StarredFragment extends Fragment implements AbsListView.OnItemClickListener {

    // TODO: Rename and change types of parameters
    private int gridPosition;

    private OnFragmentInteractionListener mListener;

    /**
     * The fragment's ListView/GridView.
     */
    private ListView mListStarred;

    /**
     * The Adapter which will be used to populate the ListView/GridView with
     * Views.
     */
    private StarredAdapter mStarredAdapter;

    // TODO: Rename and change types of parameters
    public static StarredFragment newInstance(int gridPosition) {
        StarredFragment fragment = new StarredFragment();
        Bundle args = new Bundle();
        args.putInt("grid_position", gridPosition);
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public StarredFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            gridPosition = getArguments().getInt("grid_position");
        }

        // TODO: Change Adapter to display your content
       /* mAdapter = new ArrayAdapter<DummyContent.DummyItem>(getActivity(),
                android.R.layout.simple_list_item_1, android.R.id.text1, DummyContent.ITEMS);*/
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.starred_fragment, container, false);

        // Set the adapter
        mListStarred = (ListView) view.findViewById(R.id.listStarred);

        AppPrefs appPrefs=new AppPrefs(getActivity());
        DashBoardData data = DashBoardActivity.listDash.get(gridPosition);
        final String pathId=data.getId();

        final ProgressDialog dialog = CustomProgressDialog.getCustomProgressDialog(getActivity());
        dialog.show();


        RestClient.get(getActivity()).getStarred(appPrefs.getAuthCode(),pathId, new Callback<StarredResponse>() {

            @Override
            public void success(StarredResponse starredResponse, Response response) {
                Log.i("", "SuccessMessage" + starredResponse.toString());
                // success!

                List<StarredChatSenderData> starredDataList = starredResponse.getData();


                Log.v("Starred", "Values in starred" + starredDataList);


                mStarredAdapter = new StarredAdapter(getActivity(),starredDataList,pathId);
                mListStarred.setAdapter(mStarredAdapter);
                dialog.hide();
                dialog.dismiss();

            }

            @Override
            public void failure(RetrofitError error) {
                dialog.hide();
                dialog.dismiss();
                GiveErrorSortIt.SortError(error, getActivity());
                Log.e("DashBoard Activity", error.getLocalizedMessage());

                // something went wrong
            }
        });
        // Set OnItemClickListener so we can be notified on item clicks
        mListStarred.setOnItemClickListener(this);

        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (null != mListener) {
            // Notify the active callbacks interface (the activity, if the
            // fragment is attached to one) that an item has been selected.
            mListener.onFragmentInteraction(DummyContent.ITEMS.get(position).id);
        }
    }

    /**
     * The default content for this Fragment has a TextView that is shown when
     * the list is empty. If you would like to change the text, call this method
     * to supply the text it should use.
     */
   /* public void setEmptyText(CharSequence emptyText) {
        View emptyView = mListView.getEmptyView();

        if (emptyView instanceof TextView) {
            ((TextView) emptyView).setText(emptyText);
        }
    }*/

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(String id);
    }

}
