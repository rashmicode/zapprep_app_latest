package in.zapprep.ZapPrep;

import java.util.List;

/**
 * Created by PoolDead on 7/6/2018.
 */

public class GetReportDataResponse {
    List<GetReportData> data;

    public List<GetReportData> getData() {
        return data;
    }

    public void setData(List<GetReportData> data) {
        this.data = data;
    }
}
