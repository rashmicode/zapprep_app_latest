package in.zapprep.ZapPrep;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.koushikdutta.ion.Ion;


/**
 * Created by Akshay on 5/15/2015.
 */
public class ExploreCourse_MentorDesc extends Fragment {

    TextView name, desc, title, mentorHandle;
    ImageView profPic;
    ProfileData pd;
    String flag;
    //List<MarketData> data = MarketPlaceAdapter.list;
    MarketData marketData = ExploreCourse.exploreCourseData;
    String mPathId;

    public static ExploreCourse_MentorDesc newInstance(String pathID) {
        ExploreCourse_MentorDesc fragment = new ExploreCourse_MentorDesc();
        Bundle args = new Bundle();
        args.putString("pathID", pathID);
        fragment.setArguments(args);
        return fragment;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mPathId = getArguments().getString("pathID");
            Log.e("", "PathId in course_detail page" + mPathId);
        }
        // mPathId=getActivity().getIntent().getExtras().getString("pathID");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.lay_mentor_desc, container, false);
        name = (TextView) view.findViewById(R.id.tv_ec_md_mentorName);
        title = (TextView) view.findViewById(R.id.tv_ec_md_subHead);
        desc = (TextView) view.findViewById(R.id.tv_ec_md_body);
        profPic = (ImageView) view.findViewById(R.id.iv_mentor_imageP);
        mentorHandle = (TextView) view.findViewById(R.id.tv_ec_md_mentorHandle);

        name.setTypeface(ApplicationSingleton.robotoRegular);
        title.setTypeface(ApplicationSingleton.robotoRegular);
        desc.setTypeface(ApplicationSingleton.robotoRegular);
        mentorHandle.setTypeface(ApplicationSingleton.robotoRegular);

        name.setText(marketData.getProducer().getName());
        mentorHandle.setText("@" + marketData.getProducer().getUsername());
        if (marketData.getProducer().getDesc() != null) {
            desc.setText(marketData.getProducer().getDesc());
        }
        if (marketData.getProducer().getTagline() != null) {
            title.setText(marketData.getProducer().getTagline());
        }

        Glide.with(getActivity())
                .load(marketData.getProducer().getAvatar())
                .centerCrop()
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.placeholder)
                .crossFade()
                .dontAnimate()
                .into(profPic);
       /* Ion.with(profPic)
                .placeholder(R.drawable.loading)
                .error(R.drawable.exclamation_error)
                .load(marketData.getProducer().getAvatar());*/
        return view;
    }
}
