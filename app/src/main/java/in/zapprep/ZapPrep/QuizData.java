package in.zapprep.ZapPrep;

import java.util.Arrays;

/**
 * Created by PoolDead on 7/3/2018.
 */

public class QuizData {
    public String concept;
    public String desc;
    public String duration;
    public String negativeMark;
    public String path;
    public String practiceCount;
    public String producer;
    public String questions[];
    public String quizType;
    public String title;
    public String type;
    public String pos;

    public String getPos() {
        return pos;
    }

    public void setPos(String pos) {
        this.pos = pos;
    }

    public String getQuizType() {
        return quizType;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getConcept() {
        return concept;
    }

    public void setConcept(String concept) {
        this.concept = concept;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getNegativeMark() {
        return negativeMark;
    }

    public void setNegativeMark(String negativeMark) {
        this.negativeMark = negativeMark;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getPracticeCount() {
        return practiceCount;
    }

    public void setPracticeCount(String practiceCount) {
        this.practiceCount = practiceCount;
    }

    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public String[] getQuestions() {
        return questions;
    }

    public void setQuestions(String[] questions) {
        this.questions = questions;
    }

    public String isQuizType() {
        return quizType;
    }

    public void setQuizType(String quizType) {
        this.quizType = quizType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "QuizData{" +
                "concept='" + concept + '\'' +
                ", desc='" + desc + '\'' +
                ", duration=" + duration +
                ", negativeMark=" + negativeMark +
                ", path='" + path + '\'' +
                ", practiceCount=" + practiceCount +
                ", producer='" + producer + '\'' +
                ", questions=" + Arrays.toString(questions) +
                ", quizType=" + quizType +
                ", title='" + title + '\'' +
                '}';
    }
}
